import express from 'express';
import { isLoggedIn } from './guards';
// import { userController } from './main';
import { reportController, reportResultController } from './main';
import multer from 'multer'

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/uploads/report`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

const uploadReport = multer({ storage });
export const reportRoutes = express.Router();
// // User routes
// routes.get('/login/google',userController.loginGoogle);
// routes.post('/login',userController.login);
// routes.get('/logout',userController.logout);

// Report routes
reportRoutes.post('/report-data', isLoggedIn, uploadReport.single('file'),reportController.reportData)
reportRoutes.post('/report-data-pic', isLoggedIn, uploadReport.single('file'),reportController.reportPhoto)
reportRoutes.get('/report-data-add', isLoggedIn,reportController.addReportParameter)
reportRoutes.get('/report/result', reportResultController.reportResultCatName)
reportRoutes.get('/report/result/data', reportResultController.reportResultCatData)
reportRoutes.get('/report/result/data/report', reportResultController.getChartReportData)
reportRoutes.get('/param-range', reportResultController.getParamRange)
reportRoutes.post('/report-data', uploadReport.single('file'),reportController.reportData)
reportRoutes.post('/report-data-pic', isLoggedIn, uploadReport.single('file'),reportController.reportPhoto)
reportRoutes.get('/report/result/supplement', reportResultController.supplementGeneration)
