import Knex from 'knex';


export interface NewsFeed {

}

export class NewsFeedService {
    constructor(private knex: Knex) { }

    public getNewsFeed = async () => {
        const newsFeeds = await this.knex('news')
            .select('*')

        
        return newsFeeds;
    }
}