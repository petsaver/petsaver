import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["test"]); // Now the connection is a testing connection.
import {UserService} from './UserService';

describe("UserService",()=>{

    let userService:UserService;

    beforeEach(async ()=>{
        userService = new UserService(knex);
        await knex('user').del();
        await knex('district').del();
        await knex.insert({
            id:'1',
            district:'Tuen Mun'
        }).into('district');
        await knex.insert({
            username:"ksud",
            password:"123321",
            email:"ksud@tecky.io",
            district_id:"1",
            phone_no:'72183164'
        }).into('user');
    })

    it("should get all user", async () => {
        const user = await userService.getUser('ksud@tecky.io');
        console.log (user)
        expect(user.length).toBe(1);
    });

    afterAll(()=>{
        knex.destroy();
    });
})
