import Knex from 'knex';

export class ReportService {
    constructor(private knex: Knex) {

    }

    public insertClinicData = async (clinic_name: any, clinic_phone_no: any, vet_name: any, cat_id: any, report_date: any) => {
        return await this.knex('blood_test_report')
            .insert({ clinic_name, clinic_phone_no, vet_name, cat_id, report_date })
            .returning('id')
    }

    public insertReportData = async (body: object, bloodTestReportId: any) => {
        // console.log(bloodTestReportId);

        let bloodTestParamIds = await this.knex('blood_test_parameters').select('*')
        let supplement_array = [];
        let report_data_array = [];
        // console.log("bloodTestParamsId", bloodTestParamIds);

        for (let param in body) {
            // Review
            let filter = bloodTestParamIds.filter(el => el.parameter == param)
            // console.log(filter);

            if (filter.length !== 0) {
                // console.log("filter : " + filter[0].id);
                // console.log([filter[0].id ,body[param]]);
                // console.log("body param" + typeof (body[param]));
                let value: number;
                value = typeof (body[param]) === "string" ? parseFloat(body[param]) : parseFloat(body[param][0]);
                // console.log("value " + typeof (value) + value);

                if (!isNaN(value)) {
                    // console.log (value)
                    report_data_array.push({
                        blood_test_parameters_id: filter[0].id,
                        value: value,
                        blood_test_report_id: parseInt(bloodTestReportId)
                    })
                }
                console.log ("param, value" ,param, value)
                let supplementIds = await this.knex.select("supplement_id")
                    .from("blood_test_parameters")
                    .where("blood_test_parameters.parameter", param)
                    .andWhere(function () {
                        this.where("smallerThan", ">", value)
                            .orWhere("largerThan", "<", value)
                    })
                    console.log ("supplementIds",supplementIds)
                if (supplementIds[0]) {
                    supplement_array.push({ blood_test_report_id: parseInt(bloodTestReportId), supplement_id: parseInt(supplementIds[0].supplement_id) })
                }
            }

        }
        await this.knex("report_data")
            .insert(report_data_array)

        await this.knex("blood_test_report_supplement")
            .insert(supplement_array)
    }

    public selectReportData = async () => {
        // console.log(bloodTestReportId);
        let bloodTestParamIds = await this.knex('blood_test_parameters').select('*')
        return bloodTestParamIds
    }
}
