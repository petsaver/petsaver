import Knex from 'knex';

export class ReportResultService {
    constructor(private knex: Knex) {

    }

    public getCatName = async (id: number) => {
        let catName = await this.knex('blood_test_report')
        .select('*')
        .innerJoin('cat', 'cat.id', 'blood_test_report.cat_id')
        .where('blood_test_report.id',id)
        return catName;
    }

    public getCatOrganData = async (id: number)=>{
        let catOrganData = await this.knex('report_data')
        .select('value', 'blood_test_report_id','blood_test_parameters_id','blood_test_parameters.parameter')
        .innerJoin('blood_test_parameters', 'blood_test_parameters.id', 'blood_test_parameters_id')
        .where('blood_test_report_id', id)
        console.log("service",catOrganData)
        return catOrganData;

    }

    public selectParamRange = async () => {
        return await this.knex('blood_test_parameters')
            .select('parameter', 'largerThan', 'smallerThan')
    }
    
    public getSupplement = async (bloodTestReportId:number) => {
        let supplementInfo = await this.knex('supplement')
        .select("*")
        .innerJoin("blood_test_report_supplement", "supplement_id", "supplement.id")
        .where("blood_test_report_supplement.blood_test_report_id", bloodTestReportId)
        return supplementInfo
    }

    public getChartReportData = async (id: number) => {
        console.log("id", id)

         let catId = await this.knex('blood_test_report')
         .where('id', id)
         .select('cat_id')
        console.log("catId", catId)

        let chartReportData = await this.knex('report_data')
        .select('report_data.value', 'blood_test_report_id','blood_test_parameters_id','blood_test_parameters.parameter','blood_test_report.cat_id')
        .innerJoin('blood_test_parameters', 'blood_test_parameters.id', 'report_data.blood_test_parameters_id')
        .innerJoin('blood_test_report', 'blood_test_report.id', 'report_data.blood_test_report_id')   
        .where('blood_test_report.cat_id', catId[0].cat_id)
        .whereIn('parameter', ['ALKP','ALT','BUN','CREA','TBIL','AMYL'])
        .orderBy('parameter','asc')
        .orderBy('blood_test_report_id', 'asc')

        console.log("chartReportData",chartReportData)
        return chartReportData;
    }
}

// SELECT
// value,
// blood_test_report_id,
// blood_test_parameters_id,
// blood_test_parameters.parameter
// FROM
// report_data
// INNER JOIN
// blood_test_parameters on 
// blood_test_parameters.id = 
// report_data.blood_test_parameters_id 
// WHERE 
// blood_test_report_id=10;