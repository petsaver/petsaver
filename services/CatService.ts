import Knex from 'knex';

export interface Cat {
    id: number,
    name: string,
    gender: string,
    date_of_birth: string,
    blood_type: string,
    breed: string
    filename: string
}

export class CatService {
    constructor(private knex: Knex) {
    }

    public getAllCat = async (id: number) => {
        let result = await this.knex.select('cat.*', 'blood_type.type').from('cat')
        .innerJoin('blood_type', 'cat.blood_type_id','blood_type.id')
        .where('user_id', id).andWhere("is_delete", "f");
        return result
    }

    public getCatById = async (id: string) => {
        let result = await this.knex.select('cat.*', 'blood_type.type').from('cat')
        .innerJoin('blood_type', 'cat.blood_type_id','blood_type.id')
        .where('cat.id', id);
        return result
    }

    public getBlood_type = async () => {
        let blood_types = await this.knex.select("type").from('blood_type')
        return blood_types
      }

    public getCatReportCount = async (id: number) => {
        // let result = await this.knex('cat')
        //     .select('cat.id', 'cat.name', 'cat.pet_pic','cat.gender','cat.date_of_birth','cat.is_delete','cat.created_at')
        //     .count('blood_test_report.id')
        //     .join('blood_test_report', 'cat.id', 'blood_test_report.cat_id')
        //     .groupBy('cat.id', 'cat.name', 'cat.pet_pic','cat.gender','cat.date_of_birth','cat.is_delete','cat.created_at')
        //     .orderBy('cat.created_at', 'desc')
        //     .where('user_id', id)
        let result = await this.knex('blood_type')
        .select('*')
        .join('cat', 'cat.blood_type_id', 'blood_type.id')
        .orderBy('cat.created_at','desc')
        .where('user_id',id)
        return result
    }
    public getAllCatWithReport = async (id: number) => {
        let result = await this.knex('cat')
            .select('*')
            .fullOuterJoin('blood_test_report','blood_test_report.cat_id', 'cat.id' )
            .where('user_id', id)
            .orderBy('report_date','desc')
        // let result = await this.knex('blood_test_report')
        //     .select('*')
        //     .where('cat_id', id)

        return result
        
    }

    public createCat = async (id:number, name: string, gender: string, date_of_birth: string, blood_type: string, breed: string, filename: string) => {
        let blood_typeId = await this.knex.select("id").from('blood_type').where('type', blood_type )
        let result = await this.knex.insert({
            user_id:id,
            name: name,
            gender: gender,
            date_of_birth: date_of_birth,
            blood_type_id: blood_typeId[0].id,
            pet_pic: filename,
            breed: breed
        }).into('cat').returning('id');

        return result;
    }

    public updateCat = async (id: string, catName: string, gender: string, catYear: string, catBlood_type: string, catBreed: string, filename: string) => {
        let blood_typeId = await this.knex.select("id").from('blood_type').where('type', catBlood_type)
        let updateInfo = {
            name: catName,
            gender: gender,
            date_of_birth: catYear,
            blood_type_id: blood_typeId[0].id,
            breed: catBreed,
        }
        if (filename !== "undefined"){
        updateInfo["pet_pic"] = filename
        }
        let result = await this.knex('cat').update(
            updateInfo
        ).where('id', id);
        
        return result;
    }

    public deleteCat = async (id:string) => {
        await this.knex('cat').update({
            is_delete: "t"
        }).where('id', id)
    }

}