import { ReportService } from './ReportService';
import {CatService} from './CatService'
import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["testing"]);

describe("ReportService", () => {
    let reportService: ReportService;

    beforeEach(async () => {
        await knex.migrate.rollback();
        await knex.migrate.latest();
        await knex.seed.run();
        reportService = new ReportService(knex);
        catService = new CatService(knex)
    })

    it("should insert cat clinic data", async () => {
        let body = {
            clinic_name: "tecky",
            clinic_phone_no: "21800000",
            vet_name: "alex",
            cat_id: 10,
            report_date: "2020-11-20",
        }
        const clinic_data = await reportService.insertClinicData(body.clinic_name,body.clinic_phone_no,body.vet_name,body.cat_id,body.report_date)
        const get_clinic_data = await reportService.selectReportData


    })

})
afterAll(() => {
    knex.destroy()
})