import Knex from 'knex';
import { hashPassword } from '../hash';
import fetch from 'node-fetch';

export interface User {
  id: number,
  username: string,
  password: string
}
export class UserService {
  constructor(private knex: Knex) {
  }

  public getUser = async (email: string) => {
    let rows = await this.knex("user")
      .innerJoin('district', 'user.district_id', 'district.id')
      .select('user.*', 'district.district')
      .where('email', email)
    console.log(rows);

    return rows
  }
  public getUserByEmail = async (email: string) => {
    let rows = await this.knex("user")
      // .innerJoin('district', 'user.district_id', 'district.id')
      .select("*")
      .where('email', email)
    console.log(rows);

    return rows
  }

  public getDistricts = async () => {
    let districts = await this.knex.select("district").from('district')
    return districts
  }

  public getUserByIdWithDistrict = async (id: number) => {
    let rows = await this.knex('user')
      .innerJoin('district', 'user.district_id', 'district.id')
      .select('user.*', 'district.district')
      .where('user.id', id)
    // console.log(rows);

    return rows[0]
  }
  public getUserById = async (id: number) => {
    let rows = await this.knex('user')
      .select('*')
      .where('user.id', id)
    // console.log(rows);

    return rows[0]
  }


  public createUser = async (username: string, email: string, password?: string, phone_no?: string, district?: number) => {
    let hash = null;
    if (password) {
      hash = await hashPassword(password);
    }
    // query builder
    if (district) {
      let districtId = await this.knex.select('id').from('district').where('district', district);

      let result = await this.knex.insert({
        username: username,
        password: hash,
        email: email,
        phone_no: phone_no,
        district_id: districtId[0].id
      }).into('user').returning('id');

      return result;
    } else {
      let result = await this.knex.insert({
        username: username,
        password: hash,
        email: email,
        phone_no: phone_no,
      }).into('user').returning('id');
      return result
    }
  }

  public updateUser = async (id: number, username: string, password: string, phone_no: string, district: string) => {
    let hash = await hashPassword(password);

    let districtId = await this.knex('district').update({
      district: district
    }).where('district.id', 'user.district_id').returning('id')

    let result = await this.knex('user').update({
      username: username,
      password: hash,
      phone_no: phone_no,
      district_id: districtId
    }).where('user.id', id)

    return result;
  }

  public getUserFromGoogle = async (accessToken: string) => {
    const fetchRes = await fetch(
      'https://www.googleapis.com/oauth2/v2/userinfo',
      {
        method: 'get',
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      }
    );
    let googleUser = await fetchRes.json();
    let email = googleUser.email
    console.log("abc", email);

    return email;
  }






}