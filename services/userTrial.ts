import { UserService } from './UserService';
import Knex from 'knex';
const knexfile = require('../knexfile');
const knex = Knex(knexfile["test"]); 

const userService = new UserService(knex);

// WARNING: do not run this file - very violent

const myFunction = async () => {

    await knex('user').del();
    await knex('district').del();

    await knex.insert({
        username:"ksud",
        password:"123321",
        email:"ksud@tecky.io",
        district_id:"1",
        phone_no:'72183164'
    }).into('user');

    await knex.insert({
        district:'Tuen Mun'
    }).into('district');

    const user = await userService.getUser('ksud@tecky.io');
    console.log (user)
    expect(user.length).toBe(1);
}

myFunction();
