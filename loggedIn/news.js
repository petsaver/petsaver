console.log("abc")
const newsFeedDiv = document.querySelector('.news-feed')
const newsFeed = async () => {
    const res = await fetch('/newsfeed');
    newsFeedJSON = await res.json();
    displayNewsFeed(newsFeedJSON);
    // console.log(newsFeedJSON);

};
const displayNewsFeed = (events) => {
    // console.log(events);
    const htmlString = events
        .map((event) => {
            const news = event
            return `
        <div class="news-card">
        <div class="news-image">
            <img src="${news?.urlToImage}">
        </div>
        <div class="news-content">
            <h3 class="news-title">${news.title}</h3>
            <a href="${news.url}" class="news-source">${news.source}, By <span class="news-author">${news.author}</span></a>
            <hr>
            <div class="news-body">
                <p class="news-text" id="description">${news.description}</p>
            <hr>
                <div class="publish-time">
                    <div>${news.publishedAt}</div>
                </div>
            </div>
        </div>
    </div>
        `;
        })
        .join('');
    newsFeedDiv.innerHTML = htmlString;
}
newsFeed()