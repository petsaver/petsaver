
loadUserProfile()
loadCatName()

// const cropperImage = document.getElementById('cropper-image');
// const cropper = new Cropper(cropperImage, {
//   aspectRatio: 16 / 9,
//   crop(event) {
//     console.log(event.detail.x);
//     console.log(event.detail.y);
//     console.log(event.detail.width);
//     console.log(event.detail.height);
//     console.log(event.detail.rotate);
//     console.log(event.detail.scaleX);
//     console.log(event.detail.scaleY);
//   },
// });


async function loadUserProfile() {

    let resUserData = await fetch('/profile')
    // console.log(resUserData);
    let resUserDistrictData = await fetch('/profile/district')
    let userData = await resUserData.json();
    console.log(resUserDistrictData);
    let userDistrictData = resUserDistrictData === undefined ? null : await resUserDistrictData.json()

    document.querySelector('.info-box').innerHTML = ""

    let userId = userData.id
    let userName = userData.username
    let userEmail = userData.email
    let userPhone_no = userData.phone_no
    let userDistrict = userDistrictData.district

    document.querySelector('.info-box').innerHTML += `
          <div class="card card-container">
          <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
          <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
          <p id="profile-name" class="profile-name-card"></p>
          <!-- <div class='background'> -->
              <form action="/updateProfile" method="PUT" class="form-register">
                  <span id="reauth-email" class="reauth-email"></span>
                  <div class="inputbox">
                  <div class="inputbox-item">
                  <i class="far fa-user"></i>
                  </div>
                  <input type="text" id="inputUserName" name="catName" value=${userName} class="form-control"
                    required >
                    </div>
                <div class="inputbox">
                <div class="inputbox-item">
                <i class="far fa-envelope"></i>
                </div>
                <input type="text" id="inputEmail" name="catEmail" value=${userEmail} class="form-control" disabled>
                </div>
                <div class="inputbox">
                <div class="inputbox-item">
                <i class="fas fa-mobile-alt"></i>
                </div>
                <input type="text" id="inputPhone_no" name="catYear" value=${userPhone_no} class="form-control"
                    required>
                </div>
                <div class="inputbox">
                <div class="inputbox-item">
                <i class="far fa-building"></i>
                </div>  
                        <select id="districts" class="custom-select form-control form-register"> 
                        </select>
                </div>    
                  <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" type="submit">Update</button>
                  </form><!-- /form -->
          <!-- </div> -->
      </div><!-- /card-container -->
    `

    let districtsData = await fetch('/district')
    let districts = await districtsData.json()

    let districtHTML = document.getElementById("districts")

    districtHTML.innerHTML = ""
    for (let j = 0; j < districts.length; j++) {
        if (userDistrict == districts[j].district) {
            districtHTML.innerHTML += `<option selected class="${districts[j].district}">${districts[j].district}</option>`
        } else {
            districtHTML.innerHTML += `<option class="${districts[j].district}">${districts[j].district}</option>`
        }
    }
}
async function loadCatName() {
    let resCatData = await fetch('/catprofile');
    let catData = await resCatData.json();
    document.querySelector('.cat-profile-button').innerHTML = ""

    for (let i = 0; i < catData.length; i++) {
        let catId = catData[i].id
        let catName = catData[i].name
        document.querySelector('.cat-profile-button').innerHTML += `
        <a class="profile-button" onclick="loadCatProfile(${catId})">${catName}'s Profile</a>
        `
    }
}
async function loadCatProfile(id) {
    let resCatData = await fetch(`/catprofile/${id}`);
    let catData = await resCatData.json();
    document.querySelector('.info-box').innerHTML = ""

    let catId = catData[0].id
    let catName = catData[0].name
    let catGender = catData[0].gender
    let catYear = catData[0].date_of_birth.substring(0, 10)
    let catBreed = catData[0].breed
    let catBloodType = catData[0].type
    let catPic = catData[0].pet_pic

    let checkedMale = ""
    let checkedFemale = ""
    if (catGender == 'Male') {
        checkedMale = "checked"
    } else {
        checkedFemale = "checked"
    }

    document.querySelector('.info-box').innerHTML = `
    

    <div class="card card-container cat${catId}">
        
          <!-- <div class='background'> -->
        <form class="form-register" id="update-cat">
        <div class="upload-box">
            <label class="upload_cover">
              <input id="upload_input" type="file" name="cat_pic" onchange="previewFile()">
              <span class="upload_icon"><img class="cat-pic" src="uploads/${catPic}">
              </span>
              <i class="delAvatar fa fa-times-circle-o" title="刪除"></i>
            </label>
        </div>
        <div class="inputbox">
                <div class="input-item">
                   <i class="fas fa-id-badge"></i>
                </div>
                <input type="text" id="inputcatName" name="catName" value=${catName} class="form-control"  required >
        </div>
        <div class="inputbox">
                <div class="input-item">
                    <i class="fas fa-birthday-cake"></i>
                </div>
                    <input type="date" id="inputcatYear" name="catYear" value=${catYear} class="form-control"  required>
        </div>
                   
        <div class="custom-gender">
            <div class="male">
                    <input type="radio" id="catGenderMale" name="gender" value="Male" ${checkedMale}>
                    <label for="contactChoice1"><i class="fas fa-mars"></i></label>
            </div>
            <div class="female">
                    <input type="radio" id="catGenderFemale" name="gender" value="Female" ${checkedFemale}>
                    <label for="contactChoice2"><i class="fas fa-venus"></i></label>
            </div>
        </div>
                
        <div class="inputbox">
            <div class="inputbox-item">
                <i class="fas fa-cat"></i>
            </div>
                <input type="text" id="inputcatBreed" name="catBreed" value=${catBreed} class="form-control" brequired>
        </div>
        <div class="inputbox">
            <div class="inputbox-item">
                <i class="fas fa-syringe"></i>
            </div>
                <select id="blood_type_${catId}" class="custom-select form-control" name="catBlood_type">
                </select>
        </div>
        <div class="message-box">
        </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" type="submit" onclick="updateCat(event, ${catId})">Update</button>
        </form>
        <form class="delete-button form-register">
            <button class="btn btn-lg btn-primary btn-block btn-signin" id="delete-button" onclick="deleteCat(${catId})">Delete</button>
        </form>
    </div><!-- /card-container -->
                  `

    let blood_typesData = await fetch('/catBlood_type');
    let blood_types = await blood_typesData.json();

    let blood_typeHTML = document.getElementById(`blood_type_${catId}`);

    blood_typeHTML.innerHTML = ""
    for (let m = 0; m < blood_types.length; m++) {
        if (catBloodType == blood_types[m].type) {
            blood_typeHTML.innerHTML += `<option selected class="${blood_types[m].type}" >${blood_types[m].type}</option>`
        } else {
            blood_typeHTML.innerHTML += `<option class="${blood_types[m].type}">${blood_types[m].type}</option>`
        }
    }


}

async function addCatProfile() {

    document.querySelector('.info-box').innerHTML = `
    <div class="card card-container addcat">
        <form action="/createcat" method="POST" class="form-register" enctype="multipart/form-data" >
        <div class="upload-box">
        <label class="upload_cover">
            <input id="upload_input" type="file" name="cat_pic" onchange="previewFile()" required>
            <span class="upload_icon"><img class="cat-pic" src="https://cdn2.iconfinder.com/data/icons/rounded-white-basic-ui-set-3/139/Photo_Add-RoundedWhite-512.png" alt="Placeholder"></span>
            <i class="delAvatar fa fa-times-circle-o" title="刪除"></i>
        </label>

        </div>
            <div class="inputbox">
                <div class="input-item">
                  <i class="fas fa-id-badge"></i>
                  </div>
                <input type="text" id="inputcatName" name="name" accept="image/*" class="form-control" placeholder="Cat's Name"
                    required >
            </div>

            <div class="inputbox">
                <div class="input-item">
                <i class="fas fa-birthday-cake"></i>
                </div>
                <input type="date" id="inputcatYear" name="date_of_birth" class="form-control"
                    required>
            </div>

            <div class="custom-gender">
                <div class="male">
                    <input type="radio" id="catGenderMale" name="gender" value="Male" >
                    <label for="contactChoice1"><i class="fas fa-mars"></i></label>
                </div>
                <div class="female">
                    <input type="radio" id="catGenderFemale" name="gender" value="Female">
                    <label for="contactChoice2"><i class="fas fa-venus"></i></label>
                </div>
            </div>

            <div class="inputbox">
                <div class="inputbox-item">
                <i class="fas fa-cat"></i>
                </div>
                <input type="text" id="inputcatBreed" name="breed"  class="form-control" placeholder="Cat's Breed" 
                required>
            </div>

            <div class="inputbox">
                <div class="inputbox-item">
                <i class="fas fa-syringe"></i>
                </div>
                <select id="blood_type" class="custom-select form-control" name="blood_type">
                    <option disabled selected>Blood-type</option>
                    <option>A</option>
                    <option>B</option>
                    <option>AB</option>
                </select>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" type="submit">Create</button>
        </form><!-- /form -->
    </div>
    `
}

function previewFile() {
    let upload_input = document.querySelector("#upload_input")
    let previewImg = document.querySelector(".upload_icon")
    console.log (upload_input.files)
    let file = upload_input.files[0];
    if (file) {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.addEventListener("load", async function () {
            previewImg.innerHTML = '<img class="cat-pic" src="' + this.result + '" />';
        })
    }

}

async function updateCat(event, id) {
    event.preventDefault()
    let updateForm = document.querySelector("#update-cat")
    let formData = new FormData()

    formData.append('catName', updateForm.catName.value);
    formData.append('catYear', updateForm.catYear.value);
    formData.append('gender', updateForm.gender.value);
    formData.append('catBreed', updateForm.catBreed.value);
    formData.append('catBlood_type', updateForm.catBlood_type.value);

    if (updateForm.cat_pic.files[0]) {
        formData.append('cat_pic', updateForm.cat_pic.files[0]);
    }

    let resCatData = await fetch(`/updatecat/${id}`, {
        method: "PUT",
        body: formData,
    });
    let result = await resCatData.json();
    document.querySelector(".message-box").innerHTML = result
}

async function deleteCat(id) {
    let resCatData = await fetch(`/deletecat/${id}`, {
        method: 'DELETE',
    });
    await resCatData.json();
    window.location.reload()
}



