let scrollGeneral = document.querySelectorAll(".general-name")
for (let i = 0; i < scrollGeneral.length; i++) {
    scrollGeneral[i].addEventListener("click", async () => {
        let generalButton = document.getElementById("organ-details-general")
        generalButton.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
    })
}

let scrollElectrolytes = document.querySelector(".electrolytes-name")
scrollElectrolytes.addEventListener("click", async () => {
    let electrolytesButton = document.getElementById("organ-details-electrolytes")
    electrolytesButton.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
})

let scrollLiver = document.getElementById("Liver")
scrollLiver.addEventListener("click", async () => {
    let liverButton = document.getElementById("organ-details-liver")
    liverButton.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
})

let scrollKidney = document.getElementById("Kidney")
scrollKidney.addEventListener("click", async () => {
    let kidneyButton = document.getElementById("organ-details-kidney")
    kidneyButton.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
})

let scrollPancreas = document.getElementById("Pancreas")
scrollPancreas.addEventListener("click", async () => {
    let PancreasButton = document.getElementById("organ-details-pancreas")
    PancreasButton.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
})

let scrollGallBladder = document.getElementById("Gallbladder")
scrollGallBladder.addEventListener("click", async () => {
    let gallButton = document.getElementById("organ-details-gallbladder")
    gallButton.scrollIntoView({ behavior: "smooth", block: "center", inline: "nearest" })
})

async function getCatName() {
    const res = await fetch('/report/result' + location.search)
    const catName = await res.json();
    let name = document.querySelector('.cat-info')
    name.innerHTML = `
        <div class="cat-info-name">Name</div>
        <div style="text-transform: capitalize;">${catName}</div>
        `
}

getCatName()

async function getCatOrganCondition() {
    const res = await fetch('/report/result/data' + location.search)
    const organCondition = await res.json();
    console.log("Result-record.js: getCatOrganCondition() - running fetch")
    console.log("Result-record.js: await res.json() ", organCondition);

    let res1 = await fetch('/param-range')
    let params = await res1.json()
    console.log("Result-record.js: getCatOrganCondition() - await res1.json() ",params);

    for (let el of organCondition) {
        for (let param of params) {
            let organ;
            if (param.parameter == "ALKP" || param.parameter == "ALT" || param.parameter == "CHOL" || param.parameter == "GLOB" || param.parameter == "ALB") { organ = "Liver" };
            if (param.parameter == "BUN" || param.parameter == "CREA" || param.parameter == "PHOS" || param.parameter == "UREA") { organ = "Kidney" };
            if (param.parameter == "AMYL") { organ = "Pancreas" };
            if (param.parameter == "TBIL") { organ = "Gallbladder" };
            if (param.parameter == "CA") { organ = "Calcium" };
            if (param.parameter == "GLU") { organ = "Glucose" };
            if (param.parameter == "TP") { organ = "total-protein" };
            if (param.parameter == "CL" || param.parameter == "K" || param.parameter == "NA"){ organ = "Electrolytes" };
            // console.log("1",param.parameter ,"for-loop",organ);
            checkOrgan(el, organ, param.parameter, parseFloat(param.smallerThan), parseFloat(param.largerThan))
        }
    }
}


function checkOrgan(el, organ, param, min, max) {
    // console.log("1.5", el.parameter, param)
    if (el.parameter == param) {
        // console.log("2",`${el.parameter}: max:${max}, min:${min}, value:${el.value}`);
        if (parseFloat(el.value) > max || parseFloat(el.value) < min) {
            // console.log("2.5",`${el.parameter}: max:${max}, min:${min}, value:${el.value}`);
            // console.log("3 out of range", el.parameter, `#${organ}`);
            if (document.querySelector(`#${organ} > div > span`)) {
                document.querySelector(`#${organ} > div > span`).innerHTML = `😿`
            } else {
                document.querySelector(`#${organ} > span`).innerHTML = `😿`
            }
        }
    }
}
getCatOrganCondition()

getSupplement()


//Supplement Generation
async function getSupplement() {
    let res = await fetch('/report/result/supplement' + location.search )
    let supplementInfo = await res.json();
    let supplementDiv = document.querySelector(".suggested-supplement")
    console.log ('supplement', supplementInfo)
    console.log ('supplementjs', supplementDiv)
    if (supplementInfo.length == 0) {
        supplementDiv.innerHTML += `
     
        <img class="supplement-image" src="image/vitamins.png">
        <a href="https://www.amazon.com/s?k=Vitamin+b&ref=nb_sb_noss_2"  target="_blank">Vitamin B</a>
        <img class="supplement-image" src="image/vitamins.png">
       
        <a href="https://www.amazon.com/s?k=Vitamin+e&ref=nb_sb_noss_2"  target="_blank">Vitamin E</a>
        `
    } else {
        for (let i = 0; i < supplementInfo.length; i++) {
            console.log (supplementInfo[0].supplement_link)
        supplementDiv.innerHTML += `
        <img class="supplement-image" src="image/vitamins.png">
        <a href="${supplementInfo[i].supplement_link}"  target="_blank">${supplementInfo[i].supplement_list}</a>
        `
        }
    }
    // let name = document.querySelector('.cat-info')
    // name.innerHTML = `
    //     <div class="cat-info-name">Name</div>
    //     <div style="text-transform: capitalize;">${catName}</div>
    //     `
}
