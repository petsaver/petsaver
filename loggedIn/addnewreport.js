const addReport = document.querySelector('#add-report-form')
addReport.addEventListener('submit', async function (event) {
    event.preventDefault();

    // Serialize the Form afterwards
    // const form = event.target;
    const formData = new FormData(document.querySelector('#add-report-form'));

    // formData.append('clinic_name', addReport.querySelector('input[name=clinicName]').value);
    // formData.append('clinic_phone_no', addReport.querySelector('input[name=clinicPhoneNo]').value);
    // formData.append('vet_name', addReport.querySelector('input[name=vetName]').value);
    // formData.append('report_picture', addReport.querySelector('input[name=file]').file?.files[0]);
    formData.append('cat_id', parseInt(location.search.slice(1).split("&")[0].split("=")[1]));
    // formData.append('report_picture', form.file.files[0]);
    // console.log(parseInt(location.search.slice(1).split("&")[0].split("=")[1]));
    // console.log(addReport.querySelector('input[name=file]').file);
    const res = await fetch('/report-data', {
        method: "POST",
        body: formData
    });
    const result = await res.json();
    // console.log(result);
    // document.querySelector('#contact-result').innerHTML = result;
    window.location = window.location.origin + '/result-record.html?select-report=' + result.result
})

Dropzone.autoDiscover = false;
const dataColumn = document.querySelector('.report-data-selection')

$("#dZUpload").dropzone({
    dictDefaultMessage: "Drop blood report here to upload",
    url: "/report-data-pic",
    addRemoveLinks: true,
    timeout: 9999999,
    success: function (file, response) {
        file.previewElement.classList.add("dz-success");
        console.log("uploaded photo");
        dataset = response
        dataColumn.innerHTML = `<div style="text-decoration: underline;font-weight: 900;margin-bottom: 10px">PLEASE CHECK THE DATA AFTER ANALYSIS. THERE SHOULD BE ONLY NUMBERS WITHOUT UNIT. </div>`
        for (const data of dataset) {
            let value = Object.values(data)[0]
            let key = Object.keys(data)[0]
            dataColumn.innerHTML = dataColumn.innerHTML + /*html*/`
                    <div class="input-group mb-3 report-data-input">
                            <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroup-sizing-default" >${key}</span>
                            </div>
                            <input type="text" name="${key}" class="form-control" aria-label="params data" value="${value}">
                        </div>
                        `
        }
        // console.log("Successfully uploaded :" + imgName);
        console.log("finished analysis");
    },
    error: function (file, response) {
        file.previewElement.classList.add("dz-error");
    }

});

let dzMessage = document.querySelector('.dz-message')
dzMessage.innerHTML = "Drop blood report here to upload"


const addButton = document.querySelector('.add-report-parameter')

let addedParams = false
let reminded = false

addButton.addEventListener('click', async event => {
    event.preventDefault();
    const res = await fetch('/report-data-add')
    parameters = await res.json()
    if (addedParams === true) {
        if (reminded == false) {
            const reminder = document.createElement("div");
            const parentDiv = document.querySelector(".submit-btn")
            const reminderContent = document.createTextNode("Please add parameter first before adding new parameter")
            reminder.appendChild(reminderContent)
            reminder.classList.add('adding-new-parameter')
            const currentDiv = document.getElementById("after-reminder")
            parentDiv.insertBefore(reminder, currentDiv)
            reminded = true
        }
        // console.log(parameters);
    } else {
        const addInputDiv = document.createElement("div");
        addInputDiv.innerHTML = /*html*/`
        <div class="input-group mb-3 report-data-input report-select-parameter-div">
            <div class="input-group-prepend reportParamDiv">
                <select class="form-control select-report-parameter" onchange="addParamInput()">
                <option class="parameter-option">Select Parameter:</option>      
                </select>
            </div>
            <div id="inputParamValue" class="inputParamValueClass">
            </div>
        </div>
        `
        // addInputDiv.appendChild(inputContent)
        dataColumn.appendChild(addInputDiv)
        //     dataColumn.innerHTML = dataColumn.innerHTML + /*html*/ `
        // <div class="input-group mb-3 report-data-input report-select-parameter-div">
        //     <div class="input-group-prepend reportParamDiv">
        //         <select class="form-control select-report-parameter" onchange="addParamInput()"></select>
        //     </div>
        //     <div id="inputParamValue" class="inputParamValueClass">
        //     </div>
        // </div>
        // `
        const reportParameterOptions = document.querySelectorAll('.select-report-parameter')
        const htmlParameterOption = parameters.map((parameter) => {
            return /*html*/`
                    <option class="parameter-option" name="${parameter.parameter}"  value="${parameter.parameter}" data-id="${event.id}">${parameter.parameter}</option>      
             `
        })
            .join('');
        for (let reportParameterOption of reportParameterOptions) {
            // console.log(reportParameterOption);
            reportParameterOption.innerHTML = reportParameterOption.innerHTML + htmlParameterOption
        }
        addedParams = true
    }


})

function addParamInput() {
    let inputParamDivs = document.querySelectorAll("#inputParamValue")
    let inputParamValueClass = document.querySelector(".inputParamValueClass")
    let inputParamValue = document.querySelector(".select-report-parameter").value
    let reportParamDiv = document.querySelector(".reportParamDiv")
    inputParamValueClass.innerHTML = `<input type="number" step="0.01" min="0" name="${inputParamValue}" class="form-control" aria-label="params data">`
    reportParamDiv.innerHTML = `
     <span class="input-group-text" id="inputGroup-sizing-default" >${inputParamValue}</span>
    `
    for (let inputParamDiv of inputParamDivs) {
        inputParamDiv.classList.remove("inputParamValueClass")
    }
    reportParamDiv.classList.remove("reportParamDiv")
    addedParams = false
}

