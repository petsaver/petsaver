async function getChartReportData() {
    const res = await fetch('/report/result/data/report' + location.search)
    const chartReportData = await res.json();
    // console.log("result-record-chart.js: getChartReportData() ", chartReportData)

    const chartData = {}
    for (const data of chartReportData) {
        const param = data["parameter"];
        if (param in chartData) {
            chartData[param]['data'].push(parseFloat(data.value))
        } else {
            chartData[param] = {
                data: [parseFloat(data.value)]
            }
        }
    }
    // console.log("chart data: ", chartData)
    const result = [];
    for (let key in chartData) {
        result.push({
            name: key,
            data: chartData[key].data,
        })
    }
    console.log("chart result: ", result);

    let arrayData = []
    for (let a of result) {
        // console.log("a", a)
        if (a.name == "ALKP") {
            // console.log("2")
            // console.log("a data: ", a.data)
            let dataChart = {
                labels: ['report 1', 'report 2', 'report 3', 'report 4'],
                datasets: [{
                    data: a.data,
                    label: a.name,
                    borderColor: "#3e95cd",
                    fill: false
                }]
            }

            let ChartOption = {
                title: {
                    display: true,
                    text: 'ALKP (Alkaline Phosphatase)',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 200
                        }
                    }]
                },
            }

            let Diagram = document.getElementById("alkp-diagram")
            let myLineChart = new Chart(Diagram, {
                type: 'line',
                data: dataChart,
                options: ChartOption
            });

        } else if (a.name == "ALT"){
            let dataChart = {
                labels: ['report 1', 'report 2', 'report 3', 'report 4'],
                datasets: [{
                    data: a.data,
                    label: a.name,
                    borderColor: "#3e95cd",
                    fill: false
                }]
            }

            let ChartOption = {
                title: {
                    display: true,
                    text: 'ALT (Alanine Aminotransferase)',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 130
                        }
                    }]
                },
            }

            let Diagram = document.getElementById("alt-diagram")
            let myLineChart = new Chart(Diagram, {
                type: 'line',
                data: dataChart,
                options: ChartOption
            });

        } else if (a.name == "BUN"){
            let dataChart = {
                labels: ['report 1', 'report 2', 'report 3', 'report 4'],
                datasets: [{
                    data: a.data,
                    label: a.name,
                    borderColor: "#3e95cd",
                    fill: false
                }]
            }

            let ChartOption = {
                title: {
                    display: true,
                    text: 'BUN (Blood Urea Nitrogen)',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 100
                        }
                    }]
                },
            }

            let Diagram = document.getElementById("bun-diagram")
            let myLineChart = new Chart(Diagram, {
                type: 'line',
                data: dataChart,
                options: ChartOption
            });

        }else if (a.name == "CREA"){
            let dataChart = {
                labels: ['report 1', 'report 2', 'report 3', 'report 4'],
                datasets: [{
                    data: a.data,
                    label: a.name,
                    borderColor: "#3e95cd",
                    fill: false
                }]
            }

            let ChartOption = {
                title: {
                    display: true,
                    text: 'CREA (Creatinine)',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 10
                        }
                    }]
                },
            }

            let Diagram = document.getElementById("crea-diagram")
            let myLineChart = new Chart(Diagram, {
                type: 'line',
                data: dataChart,
                options: ChartOption
            });

        }else if (a.name == "AMYL"){
            let dataChart = {
                labels: ['report 1', 'report 2', 'report 3', 'report 4'],
                datasets: [{
                    data: a.data,
                    label: a.name,
                    borderColor: "#3e95cd",
                    fill: false
                }]
            }

            let ChartOption = {
                title: {
                    display: true,
                    text: 'AMYL (Amylase)',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 50
                        }
                    }]
                },
            }

            let Diagram = document.getElementById("amyl-diagram")
            let myLineChart = new Chart(Diagram, {
                type: 'line',
                data: dataChart,
                options: ChartOption
            });

        } else if (a.name == "TBIL"){
            console.log("tbil",a.data)

            let dataChart = {
                labels: ['report 1', 'report 2', 'report 3', 'report 4'],
                datasets: [{
                    data: a.data,
                    label: a.name,
                    borderColor: "#3e95cd",
                    fill: false
                }]
            }

            let ChartOption = {
                title: {
                    display: true,
                    text: 'TBIL (Total Bilirubin)',
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 30
                        }
                    }]
                },
            }

            let Diagram = document.getElementById("tbil-diagram")
            let myLineChart = new Chart(Diagram, {
                type: 'line',
                data: dataChart,
                options: ChartOption
            });

        } 
    }
}

getChartReportData()

// let alkpChartData = {
//     // labels: ['Report 1','Report 2','Report 3'],
//     datasets: [{ 
//         data: [30,"2",102],
//         label: "ALKP",
//         borderColor: "#3e95cd",
//         fill: false
//     }, {
//         backgroundColor: "#ff6384",
//         borderColor: "#ff6384",
//         data: [14,14,14],
//         label: "Normal Range",
//         fill: 'start'
//     }, {
//         backgroundColor: "blue",
//         borderColor: "blue",
//         data: [111,111,111],
//         label: "Range",
//         fill: 'end'
//     }
// ]
// }

// let chartOption = {
//     title: {
//     display: true,
//     text: 'ALKP (Alkaline Phosphatase)'
//     }
// }

// let alkpDiagram = document.getElementById("myChart")
// let myLineChart = new Chart(alkpDiagram, {
//     type: 'line',
//     data: alkpChartData,
//     options: chartOption
// });