let header = document.querySelector("header")
header.innerHTML = header.innerHTML + `
    <div class="nav-bar">
        <div id="logo">
            CatSaver
        </div>
        <div id="nav-bar-selection">
            <div id="home-button" onclick="document.location='home.html'">
                HOME
            </div>
            <div class="min-button" onclick="document.location='home.html'"><i class="fas fa-home"></i></div>
            <div id="news-button" onclick="document.location='news.html'">
                NEWS
            </div>
            <div class="min-button" onclick="document.location='news.html'"><i class="far fa-newspaper"></i></div>
            <div id="setting-button" onclick="document.location='setting.html'">
                SETTING
            </div>
            <div class="min-button" onclick="document.location='setting.html'"><i class="fas fa-cog"></i></div>
            <div id="logout-button" onclick="document.location='index.html'">
                LOGOUT
            </div>
            <div class="min-button" onclick="document.location='index.html'"><i class="fas fa-door-open"></i></div>
        </div>
    </div>
`

// let navBar = document.createElement('div')
// navBar.classList.add('nav-bar')
// let logo = document.createElement('div')
// logo.id = "logo"
// navBar.appendChild(logo)

let footer = document.querySelector("footer")
footer.innerHTML = footer.innerHTML + `
    <div class="footer-content"></div>
`