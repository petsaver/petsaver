// const selectReportByDate = document.querySelectorAll("#select-report")
const selectReportForm = document.querySelector("#select-report-form")
const catBrowseBigContainer = document.querySelector('.cat-browse-big-container')


async function showReports() {
    const loadCatRecord = async () => {
        const res = await fetch('/catrecord/');
        catRecord = await res.json();
        console.log(catRecord.length);
        if (catRecord.length === undefined || catRecord.length === 0) {
            catBrowseBigContainer.innerHTML = `<div>You don't have cat now. <span style="cursor: pointer;color: #2c10c9;text-decoration: underline;" onclick="document.location='setting.html'">Please add cat here!</span></div>`
            catBrowseBigContainer.classList.add('d-flex', 'justify-content-center')
        } else {
            displayRecords(catRecord);
        }

    };
    const loadReportsByDate = async () => {
        const res = await fetch('/catreport/');
        const res2 = await fetch('/catrecord/');
        reportLists = await res.json();
        catLists = await res2.json();
        // console.log(reportLists);
        // console.log(catLists);

        for (let j = 0; j < catLists.length; j++) {
            const reportButton = document.querySelector('.cat-browse-record-btn' + catLists[j].id)
            let selectReportByDate = document.querySelector("#select-report" + catLists[j].id)
            selectReportByDate.innerHTML = ''
            for (let i = 0; i < reportLists.length; i++) {

                if (reportLists[i].cat_id !== null && reportLists[i].cat_id === catLists[j].id) {
                    selectReportByDate.innerHTML = selectReportByDate.innerHTML + `
                        <option class="select-report-options" name="${reportLists[i].id}" value="${reportLists[i].id}" data-id="${reportLists[i].id}">${reportLists[i].report_date_moment}</option>
                        `
                    reportButton.classList.remove('no-report')
                    reportButton.classList.add('have-report')
                }

                if (i + 1 == reportLists.length && selectReportByDate.innerHTML == '') {
                    selectReportByDate.innerHTML = `
                <option class="select-report-options">No Record</option>
                `
                    reportButton.classList.remove('have-report')
                    reportButton.classList.add('no-report')

                }
            }

        }
    }

    const displayRecords = (cats) => {
        const catCard = cats
            .map((cat) => {

                return /*html*/`<div class="cat-browse-container" data-aos="zoom-out-right">
                <div class="cat-browse-card">
                <form action="/addnewreport.html" method="GET" id='add-new-report' class="add-new-report">
                <div class="cat-browse-card-top-row">
                    <div class="cat-browse-profile-pic-container">
                        <div class="cat-browse-profile-pic">
                            <img
                            src="/uploads/${cat.pet_pic}">
                        </div>
                    </div>
                    <div class="cat-browse-card-top-data">
                        <div class="cat-browse-card-cat-name"  >
                         Name:&nbsp; <input name="cat_id" value='${cat.id}' type="hidden">
                            <div id="cat-name">
                                    ${cat.name}</div>
                        </div>
                        <div class="cat-browse-card-cat-name"  >
                        Gender:&nbsp; 
                            <div id="cat-name">
                                ${cat.gender}</div>
                        </div>
                        <div class="cat-browse-card-cat-name"  >
                        Blood Type:&nbsp; 
                            <div id="cat-name">
                                ${cat.type}</div>
                        </div>
                    </div>
                </div>
                <div class="cat-browse-card-middle">
                        <div class="cat-browse-card-cat-name-middle"  >
                        Date of Birth:&nbsp; 
                        <div id="cat-name">
                                ${cat.date_of_birth_moment}</div>
                        </div>
                        <div class="cat-browse-card-cat-name-middle"  >
                        Breed:&nbsp; 
                        <div id="cat-name">
                                ${cat.breed}</div>
                        </div>
                </div>
                <div class="record-bar">Report Record:
                        <button class="add-new-report-btn" type="submit"><i class="fas fa-plus-square"></i>&nbsp; Add New Report</button>
                </div>
                        </form>
                    <form action="/result-record.html" method="GET" id="select-report-form"
                        class="check-report-container">
                        <div class="check-record-row">
                            <select id="select-report${cat.id}" class="null-report" name="select-report" data-id="${cat.id}">
                    </select>
                            <div class="button_cont" align="center"><button class="example_f cat-browse-record-btn${cat.id}" type="submit"><span>Check Report</button></div>
                        </div>
                    </form>
                </div>
            </div>`
            })
            .join('');

        catBrowseBigContainer.innerHTML = catCard;

    };
    await loadCatRecord();
    await loadReportsByDate();
}
showReports()