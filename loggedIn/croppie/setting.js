async function loadUsername() {
  const res = await fetch('/profile')
  const json = await res.json();
  console.log(json);
  userIcon.src = json.profile_pic;
}
loadUsername();

// const changeProfile = document.querySelector('.change-profile')
// logout.addEventListener('change-profile', async (event) => {
//   event.preventDefault();

const logout = document.querySelector('.logout')
logout.addEventListener('click', async (event) => {
  event.preventDefault();

  // if you wanna use fetch
  const res = await fetch('/logout', {
    method: 'POST'
  })
  window.location.href = "/index.html";
})

// await fetch('/logout', {
//   method: 'POST',
//   body: body
// })


// $('#myModal').on('shown.bs.modal', function () {
//   $('#myInput').trigger('focus')
// })


function changeProfile() {
  const modal = document.querySelector('#myModal')
  modal.style.display = "block";

  const close = document.querySelector('.close');
  close.addEventListener('click', function () {
    modal.style.display = "none";
  });

  window.addEventListener('click', function (event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  })

}
// When the user clicks on <span> (x), close the modal
let span = document.querySelector('.close');
let modal = document.querySelector('#myModal');
span.onclick = function () {
  modal.style.display = "none";
}

let uploadDemo = document.getElementById('upload-demo');
var uploadCrop = new Croppie(uploadDemo, {
  enableExif: true,
  viewport: {
    width: 200,
    height: 200,
    type: 'circle'
  },
  boundary: {
    width: 300,
    height: 300
  }
});

// $uploadCrop = $('#upload-demo').croppie({
//   enableExif: true,
//   viewport: {
//       width: 200,
//       height: 200,
//       type: 'circle'
//   },
//   boundary: {
//       width: 300,
//       height: 300
//   }
// });

// once you select the file to upload, file -> base64
function readFile() {
  let input = upload
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      uploadDemo.classList.add('ready')
      uploadCrop.bind({
        url: e.target.result
      }).then(function () {
        console.log('jQuery bind complete');
      });

    }

    reader.readAsDataURL(input.files[0]);
  }

}

let upload = document.querySelector('#upload');
upload.onchange = readFile

document.querySelector('.upload-result').onclick = async function (ev) {
    const resp = await uploadCrop.result({
        type: 'canvas',
        size: 'viewport'
    });
    // RESP is a base64 string, already a string

    // base64 -> file object

    // const formData = new FormData(); // multer understands formdata
    // TODO: append file to formData image
    // formData.append('profile_pic', resp)
    const res = await fetch('/uploadProfilePic', {
        method: 'POST',
        headers:{
          "Content-Type":"application/json"
        },
        body: JSON.stringify({
          profile_pic: resp
        })
    });
    // if (res.url) { location.href = res.url }

    window.location.reload();

    // no use
    const json = await res.json();
    reader.readAsDataURL(input.files[0]);
};

// naming incorrect
async function setProfilePic() {
    const res = await fetch('/getProfilePic');
    const json = await res.json();
    userIcon.src = json.profile_pic;
}

let userIcon = document.querySelector('#user-icon');
setProfilePic();

// can be factorized into its own js file.
async function loadUsername() {
    const res = await fetch('/profile')
    const json = await res.json();
    // console.log(json);
    const userName = document.querySelector('.username')
    userName.innerHTML = ''

    if (json.length != 0) {
        userName.innerHTML = userName.innerHTML + `
        <h6>${json.display_name}</h6>`
        console.log(json.display_name)
    }
}

loadUsername();
