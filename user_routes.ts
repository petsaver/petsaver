import express from 'express';
import { isLoggedIn } from './guards';
import { userController } from './main';


export const userRoutes = express.Router();


userRoutes.post('/signup',userController.signup);
userRoutes.get('/login/google', userController.loginWithGoogle);
userRoutes.post('/login', userController.login);
userRoutes.get('/logout',isLoggedIn, userController.logout);
userRoutes.get('/profile',isLoggedIn, userController.loadProfile);
userRoutes.get('/profile/district',isLoggedIn, userController.loadProfileDistrict);
userRoutes.get('/district',isLoggedIn, userController.getAllDistrict);
userRoutes.put('/updateProfile',isLoggedIn, userController.updateProfile)
