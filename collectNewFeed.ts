import Knex from "knex";
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])

const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('4cc87da945e24384b24b0652f7408e4e');

export async function CollectNewsFeed() {
    await knex('news').truncate()
    console.log('truncate success');
    newsapi.v2.everything({
        q: 'pets AND cat',
        sortBy: 'publishedAt',
        page: 1,
        language: 'en'
    }).then(async (response: any) => {
        for (const article of response.articles) {
            const insertNewsFeed = await knex('news')
                .insert({
                    title: article?.title,
                    source: article?.source.name,
                    author: article?.author,
                    description: article?.description,
                    url: article?.url,
                    urlToImage: article?.urlToImage,
                    publishedAt: article?.publishedAt
                }).returning(['id', 'title', 'publishedAt'])
            console.log(insertNewsFeed);
        }
        knex.destroy()
    });
}
CollectNewsFeed()