import { Request, Response } from 'express';
import { ReportService } from '../services/ReportService';
import Tesseract from 'tesseract.js';


export class ReportController {
  constructor(private reportService: ReportService) {

  }

  public reportData = async (req: Request, res: Response) => {
    // console.log('this is report data route')
    // console.log(req.body)
    // let catId = req.query.cat_id;
    let { clinic_name, clinic_phone_no, vet_name, cat_id, report_date } = req.body;
    // console.log(req.body);

    let bloodTestReportId = await this.reportService.insertClinicData(clinic_name, clinic_phone_no, vet_name, cat_id, report_date);

    let body = req.body
    // console.log(body);

    // body = {
    //   ALB: "1",
    //   ALKP: "1",
    //   ALT: "1",
    //   AMYL: '1',
    //   BUN: '1',
    //   CA: '1',
    // }

    // let ALB = req.body?.ALB
    // let ALKP = req.body?.ALKP
    // let ALT = req.body?.ALT
    // let AMYL = req.body?.AMYL
    // let BUN = req.body?.BUN
    // let CA = req.body?.CA
    // let CHOL = req.body?.CHOL
    // let CREA = req.body?.CREA
    // let GLOB = req.body?.GLOB
    // let GLU = req.body?.GLU
    // let K = req.body?.K
    // let NA = req.body?.NA
    // let PHOS = req.body?.PHOS
    // let TBIL = req.body?.TBIL
    // let TP = req.body?.TP
    // let UREA = req.body?.UREA
    // let HCL = req.body?.HCL


    // console.log(bloodTestReportId[0]);

    await this.reportService.insertReportData(body, bloodTestReportId)
    // res.redirect('/result-record.html?select-report=' + parseInt(bloodTestReportId[0]))
    res.json({ result: bloodTestReportId[0] })
  }

  public reportPhoto = async (req: Request, res: Response) => {
    try {// console.log(req.file.filename);
      await Tesseract.recognize(
        `./public/uploads/report/` + req.file.filename,
        //   'https://tesseract.projectnaptha.com/img/eng_bw.png',
        'eng',
        { logger: m => console.log(m) }
      ).then(async ({ data: { text } }) => {
        let splitText = text.split(/[\s,]+/)
        // console.log({ data: { text } });
        // console.log(splitText);
        let bloodTestParams = await this.reportService.selectReportData()
        // console.log(bloodTestParams);
        let splitTextArray = []
        for (let param of splitText) {
          let filter = bloodTestParams.filter(el => el.parameter == param)
          if (filter.length === 1) {
            let value = splitText[splitText.indexOf(filter[0].parameter) + 1]
            // console.log(filter);
            let object = {}
            object[filter[0].parameter] = value
            splitTextArray.push(object)

          }

        }
        // console.log(splitTextArray);
        res.json(splitTextArray)

      })
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }

  }

  public addReportParameter = async (req: Request, res: Response) => {
    const data = await this.reportService.selectReportData()
    res.json(data)
  }


  // public supplementGeneration = async (req:Request, res: Response) => {
  //   let body = req.body
  //   let { reportId } = req.body
  //   let supplement = await this.reportService.generateSupplement(body, reportId)
  //   res.json (supplement)
  //   }


}


