import { CatController } from "./CatController";
import { CatService } from "../services/CatService";
import Knex from "knex";

describe('Cat Controller', () => {
    let catService: CatService;
    let catController: CatController;
    let knex: Knex;

    beforeEach(() => {
        catService = new CatService(knex)
        catController = new CatController(catService)
    })

    it('load cat profile', async () => {
        const req:any = { 
            session: {
                user: {
                    id: '1',
                }
            }
        }

        const res:any = {
            json: jest.fn()
        }

        jest.spyOn(catService, 'getAllCat').mockImplementation((id)=> Promise.resolve(
            [{
                id: "1",
            }]

        ))

        await catController.loadCatProfile(req, res)

        expect(catService.getAllCat).toBeCalled();
        expect(res.json).toHaveBeenCalledWith([{id:'1'}])
    })
})