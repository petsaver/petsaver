import express from 'express';
import { Request, Response } from 'express';
import { UserService } from '../services/UserService';
import { checkPassword } from '../hash';

export class UserController {
  constructor(private userService: UserService) { }

  public loginWithGoogle = async (req: Request, res: Response) => {
    console.log("processing")
    const accessToken = req.session?.['grant'].response.access_token;
    console.log('access token:', accessToken);
    let email = await this.userService.getUserFromGoogle(accessToken);
    console.log("email:", email);

    let rows = await this.userService.getUserByEmail(email);
    console.log("rows",rows);
    
    if (rows[0] === undefined) {
      // new user
      let username = email.split('@')[0]
      let password = Math.floor(Math.random() * 999999).toString()
      console.log("username", username);
      let phone_no = "00000000".toString()
      let result = await this.userService.createUser(username, email, password, phone_no);
      let id = result;
      req.session['user'] = {
        id,
        username: username,
      };
    } else {
      // existing user
      let result = rows[0].id
      let id = result;
      let username = email.split('@')[0]
      req.session['user'] = {
        id,
        username: username,
      };
    }
    res.redirect('/home.html');
  }

  public signup = async (req: Request, res: Response) => {
    try {
      let { username, password, email, phone_no, district } = req.body;
      const existingEmail = await this.userService.getUser(email);
      if (existingEmail[0]) {
        console.log("wrong email")
        res.json("Wrong Email")
      } else {
        let id = await this.userService.createUser(username, email, password, phone_no, district);
        console.log(id);
        
        req.session["user"] = {"id": id[0]}
        
        res.json("id")
      }
    } catch (err) {
      console.log(err)
      res.status(500).json({ message: "internal server error" })
    }
  }


  public login = async (req: Request, res: Response) => {
    try {
      const { email, password } = req.body;

      const user = await this.userService.getUser(email);
      if (!user) {
        return res.status(401).redirect('/login.html?error=Incorrect+Username')
      }
      const match = await checkPassword(password, user[0].password);
      if (match) {

        if (req.session) {
          req.session['user'] = {
            id: user[0].id
          };
          console.log('logged in userID: ' + req.session['user']);
        }
        return res.redirect('/home.html'); // To the protected page.
      } else {
        return res.status(401).redirect('/login.html?error=Incorrect+Username')
      }
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }
  }

  public logout = async (req: express.Request, res: express.Response) => {
    try {
      if (req.session) {
        delete req.session['user'];
      }
      res.redirect('/');
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }
  }

  public loadProfile = async (req: Request, res: Response) => {
    try {
      let id = req.session['user'].id

      let result = await this.userService.getUserById(id)
      res.json(result);
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }
  }
  public loadProfileDistrict = async (req: Request, res: Response) => {
    try {
      let id = req.session['user'].id

      let result = await this.userService.getUserByIdWithDistrict(id)
      if (result.length === 0) {
        result = null
      }
      res.json(result);
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }
  }

  public getAllDistrict = async (req: Request, res: Response) => {
    try {
      let result = await this.userService.getDistricts()
      res.json(result);
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }
  }

  public updateProfile = async (req: Request, res: Response) => {
    try {
      let id = req.session['user'].id
      let { username, password, phone_no, district } = req.body;
      let result = await this.userService.updateUser(id, username, password, phone_no, district);
      res.json(result)
    } catch (err) {
      res.status(500).json({ message: "internal server error" })
    }
  }


}