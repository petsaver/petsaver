
import { UserController } from "./UserController";

describe('UserController', () => {
  let userService: any;

  beforeEach(() => {
    userService = {

    };
  })
  it('login with Google', async () => {
    // Arrange
    const req: any = {
      session: {
        grant: {
          response: {
            access_token: '123456'
          }
        }
      }
    };
    userService.getUserFromGoogle = jest.fn().mockResolvedValue(
      'alex@tecky.io'
    );
    userService.getUserByEmail = jest.fn().mockResolvedValue([{
      id: '1',
      username:'alex',
      email:'alex@tecky.io',
      password:'12345',
      phone_no:'12312312'
    }]);
    userService.createUser = jest.fn();
    const res: any = {
      redirect: jest.fn()
    };

    const userController = new UserController(userService)

    // Act
    await userController.loginWithGoogle(req, res);

    // Assert
    expect(userService.createUser).not.toHaveBeenCalled();
    expect(userService.getUserFromGoogle).toHaveBeenCalled();
    expect(userService.getUserByEmail).toHaveBeenCalled();
    expect(req.session['user']).toEqual({
      id: '1',
      username: 'alex'
    })
    expect(res.redirect).toHaveBeenCalledWith('/home.html');
  });

  it('signup', async () => {
    //Arrange
    const req: any = {
      body: {
          username:"Alex",
          password:'123456',
          email:'petpetksud@gmail.com',
          phone_no:'12312312',
          district:'Tuen Mun'
      }
    };

    const res: any = {
      status: jest.fn(),
      json: jest.fn()
    }

    userService.getUser = jest.fn().mockResolvedValue([
      {
        email: "petpetksud@gmail.com"
      }
    ])
    userService.createUser = jest.fn().mockResolvedValue([
      {
        id:1
      }
    ]
    )
    //Act
    const userController = new UserController(userService)
    await userController.signup(req, res)
    //Assert
    expect(userService.getUser).toHaveBeenCalled()
    expect(userService.createUser).not.toHaveBeenCalled()
    expect(res.json).toHaveBeenCalledWith("Wrong Email")
  })
  it('load profile', async () => {
    // Arrange
    const req: any = {
      session: {
        user:{
          id:"1",
          username:'petpetksud@gmail.com'
        }
      }
    };
    const res: any = {
      json: jest.fn()
    }

    const userController = new UserController(userService)
    userService.getUserById = jest.fn().mockResolvedValue({'username':"Alex", "password":"123","email":"petpetksud@gmail.com", "phone_no":"12312312"});

  
    // Act
    await userController.loadProfile(req, res);

    // Assert
    expect(req.session['user'].id).toBe('1')
    expect(userService.getUserById).toHaveBeenCalled()
    expect(res.json).toHaveBeenCalledWith({'username':"Alex", "password":"123","email":"petpetksud@gmail.com", "phone_no":"12312312"})
  })

  it('logout', () => {
    // Arrange
    const req: any = {
      session: {
        user: 'alex'
      }
    };
    const res: any = {
      redirect: jest.fn()
    }
    const userController = new UserController(userService)
    

    // Act
    userController.logout(req, res);

    // Assert
    expect(req.session.user).not.toBeDefined();
    expect(res.redirect).toHaveBeenCalledWith('/')
  })

  

})

