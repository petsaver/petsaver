import { Request, Response } from 'express';
import { ReportResultService } from '../services/ReportResultService';


export class ReportResultController {
    constructor(private reportResultService: ReportResultService) {
  
    }

    public reportResultCatName = async (req: Request, res: Response) => {
        let id: any = req.query['select-report']
        // console.log(id)
        let catName = await this.reportResultService.getCatName(parseInt(id))
        // console.log(catName)
        res.json(catName[0].name)

    }

    public reportResultCatData = async (req:Request, res: Response) => {
        let id: any = req.query['select-report']
        console.log(id)
        let catOrganData = await this.reportResultService.getCatOrganData(parseInt(id))
        console.log("data",catOrganData)
        res.json(catOrganData)
    }

    public getParamRange = async (req: Request, res: Response) => {
        let paramRange = await this.reportResultService.selectParamRange();
        res.json(paramRange)
    }

    public supplementGeneration = async (req:Request, res: Response) => {
        let id: any = req.query['select-report']
        let supplementInfo = await this.reportResultService.getSupplement(id)
        res.json(supplementInfo)
    }

    public getChartReportData = async (req:Request, res: Response) => {
        let id: any = req.query['select-report']
        console.log("getChart",id)
        let chartReportData = await this.reportResultService.getChartReportData(id)
        res.json(chartReportData)
    }
}