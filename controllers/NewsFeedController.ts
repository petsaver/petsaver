import { Request, Response } from 'express';
import moment from 'moment';
import { NewsFeedService } from '../services/NewsFeedService'

export class NewsFeedController {
    constructor(private newsFeedService: NewsFeedService) { }


    public loadNewsFeed = async (req: Request, res: Response) => {
        let newsFeeds = await this.newsFeedService.getNewsFeed()
        let newsFeedArray = []
        for (const newsFeed of newsFeeds) {
            // console.log(moment(newsFeed.publishedAt).format("MMMM Do YYYY, h:mm:ss a"));

            newsFeedArray.push({
                id: newsFeed.id,
                title: newsFeed.title,
                description: newsFeed.description,
                urlToImage: newsFeed.urlToImage,
                publishedAt: moment(newsFeed.publishedAt).format("MMMM Do YYYY"),
                source: newsFeed.source,
                author: newsFeed.author,
                url: newsFeed.url
            })
        }
        // console.log(newsFeedArray);

        res.json(newsFeedArray)

    }
}