import { Request, Response } from 'express';
import { CatService } from '../services/CatService';
import moment from 'moment';

export class CatController {
    constructor(private catService: CatService) { }

    public loadCatProfile = async (req: Request, res: Response) => {
        try {
            let id = req.session['user'].id
            let result = await this.catService.getAllCat(id)

            res.json(result);
        } catch (err) {
            res.status(500).json({ message: "internal server error" })
        }
    }

    public loadCatProfileById = async (req: Request, res: Response) => {
        let id = req.params.id
        console.log(id)
        let result = await this.catService.getCatById(id)

        res.json(result);
    }

    public loadCatRecord = async (req: Request, res: Response) => {
        try {
            let id = req.session['user'].id
            console.log('loadCatRecordByUserId : ' + id);
            let resultArray = []

            // let id = 2
            let results = await this.catService.getCatReportCount(id)
            // console.log(result);
            for (const result of results) {
                resultArray.push({
                    id: result.id,
                    name: result.name,
                    pet_pic: result.pet_pic,
                    gender: result.gender,
                    date_of_birth: result.date_of_birth,
                    date_of_birth_moment: moment(result.date_of_birth).format("MMMM Do YYYY"),
                    blood_type_id: result.blood_type_id,
                    type: result.type,
                    breed: result.breed,
                    user_id: result.user_id,
                    is_delete: result.is_delete,
                    created_at: result.created_at,
                    created_at_moment: moment(result.created_at).format("MMMM Do YYYY"),
                    updated_at: result.updated_at,
                })
            }
            res.json(resultArray);
        } catch (err) {
            res.status(500).json({ message: "internal server error" })
        }
    }

    public loadCatReport = async (req: Request, res: Response) => {
        try {
            let id = req.session['user'].id
            let resultArray = []
            let results = await this.catService.getAllCatWithReport(id)
            // console.log(results);

            for (const result of results) {
                resultArray.push({
                    id: result.id,
                    name: result.name,
                    pet_pic: result.pet_pic,
                    gender: result.gender,
                    date_of_birth: result.date_of_birth,
                    date_of_birth_moment: moment(result.date_of_birth).format("MMMM Do YYYY"),
                    blood_type_id: result.blood_type_id,
                    breed: result.breed,
                    user_id: result.user_id,
                    is_delete: result.is_delete,
                    created_at: result.created_at,
                    created_at_moment: moment(result.created_at).format("MMMM Do YYYY"),
                    updated_at: result.updated_at,
                    report_picture: result.report_picture,
                    clinic_name: result.clinic_name,
                    clinic_phone_no: result.clinic_phone_no,
                    vet_name: result.vet_name,
                    cat_id: result.cat_id,
                    report_date: result.report_date,
                    report_date_moment: moment(result.report_date).format("MMMM Do YYYY"),
                })
            }
            console.log(resultArray);
            res.json(resultArray);
        } catch (err) {
            res.status(500).json({ message: "internal server error" })
        }
    }

    public getAllBlood_types = async (req: Request, res: Response) => {
        try {
            let result = await this.catService.getBlood_type()
            res.json(result);
        } catch (err) {
            res.status(500).json({ message: "internal server error" })
        }
    }


    public createCatProfile = async (req: Request, res: Response) => {
        try {
            let id = req.session["user"].id;
            let { name, gender, date_of_birth, blood_type, breed } = req.body;

            console.log("req file" + req.file.filename)
            let { filename } = req.file
            console.log("filename" + filename)

            await this.catService.createCat(id, name, gender, date_of_birth, blood_type, breed, filename);
            res.redirect('/home.html')
        } catch (err) {
            res.status(500).json({ message: "internal server error" })
        }
    }

    public updateCatProfile = async (req: Request, res: Response) => {
        let id = req.params.id
        let { catName, gender, catYear, catBlood_type, catBreed } = req.body;
        console.log(catName)
        if (req.file == undefined) {
            var filename = "undefined"
        } else {
            var { filename } = req.file
        }

        await this.catService.updateCat(id, catName, gender, catYear, catBlood_type, catBreed, filename);
        res.json("Cat Profile has been updated")

    }

    public deleteCatProfile = async (req: Request, res: Response) => {
        let id = req.params.id
        console.log(id)
        return await this.catService.deleteCat(id)
    }
}