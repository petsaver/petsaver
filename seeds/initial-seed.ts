import * as Knex from "knex";
import bcryptjs from 'bcryptjs'

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("blood_test_report_supplement").del();
    await knex("report_data").del();
    await knex("blood_test_report").del();
    await knex("cat").del();
    await knex("user").del();
    await knex("district").del();
    await knex("blood_type").del();
    await knex("blood_test_parameters").del();
    await knex("supplement").del();


    // Inserts seed entries2020
    const districtIds = await knex("district").insert([
        { district: 'Islands' },
        { district: 'Kwai Tsing' },
        { district: 'North' },
        { district: 'Sai Kung' },
        { district: 'Sha Tin' },
        { district: 'Tai Po' },
        { district: 'Tsuen Wan' },
        { district: 'Tuen Mun' },
        { district: 'Yuen Long' },
        { district: 'Kowloon City' },
        { district: 'Kwun Tong' },
        { district: 'Sham Shui Po' },
        { district: 'Wong Tai Sin' },
        { district: 'Yau Tsim Mong' },
        { district: 'Central and Western' },
        { district: 'Eastern' },
        { district: 'Southern' },
        { district: 'Wan Chai' }
    ]).returning('id');

    const userIds = await knex("user").insert([
        { username: 'Tung Jer', password: await bcryptjs.hash('123456', 10), email: 'cw@tecky.io', phone_no: '87654321', district_id: districtIds[1] },
        { username: 'Charlie Tab', password: await bcryptjs.hash('123456', 10), email: 'ct@tecky.io', phone_no: '87654321', district_id: districtIds[7] },
        { username: 'sun.hin.gor', password: await bcryptjs.hash('123456', 10), email: 'hc@tecky.io', phone_no: '87654321', district_id: districtIds[12] },
        { username: 'Alt', password: await bcryptjs.hash('123456', 10), email: 'alt@tecky.io', phone_no: '87654321', district_id: districtIds[15] },
        { username: 'Ctrl', password: await bcryptjs.hash('123456', 10), email: 'ctrl@tecky.io', phone_no: '87654321', district_id: districtIds[14] },
        { username: 'Shift', password: await bcryptjs.hash('123456', 10), email: 'shift@tecky.io', phone_no: '87654321', district_id: districtIds[17] },
    ]).returning('id')

    const bloodTypeIds = await knex("blood_type").insert([
        { type: 'A' },
        { type: 'B' },
        { type: 'AB' },
    ]).returning('id')

    const catIds = await knex("cat").insert([
        {
            name: 'boomer', pet_pic: "ctcatpic.png",
            gender: "Male", date_of_birth: "2018-01-01", breed: "American Curl Cat Breed", is_delete: false, blood_type_id: bloodTypeIds[0],
            user_id: userIds[1]
        },
        {
            name: 'spitter', pet_pic: "ctcatpic2.jpeg",
            gender: "Female", date_of_birth: "2016-01-01", breed: "Burmese Cat", is_delete: false, blood_type_id: bloodTypeIds[0],
            user_id: userIds[1]
        },
        {
            name: 'little princess', pet_pic: "ctcatpic3.jpeg",
            gender: "Female", date_of_birth: "2016-08-06", breed: "Himalayan Cat Breed", is_delete: false, blood_type_id: bloodTypeIds[0],
            user_id: userIds[0]
        },
        {
            name: 'tank', pet_pic: "ctcatpic.png",
            gender: "Female", date_of_birth: "2016-11-20", breed: "Siamese Cat Breed", is_delete: false, blood_type_id: bloodTypeIds[2],
            user_id: userIds[2]
        },
        {
            name: 'hunter', pet_pic: "ctcatpic3.jpeg",
            gender: "Female", date_of_birth: "2016-11-20", breed: "Siamese Cat Breed", is_delete: false, blood_type_id: bloodTypeIds[2],
            user_id: userIds[1]
        },
    ]).returning('id')

    const bloodTestReportIds = await knex('blood_test_report').insert([
        { report_picture: null, clinic_name: "cat killer", clinic_phone_no: "87654321", vet_name: 'chopper', cat_id: catIds[0], report_date: new Date() },
        { report_picture: null, clinic_name: "cat killer", clinic_phone_no: "87654321", vet_name: 'chopper', cat_id: catIds[0], report_date: "2021-01-01T03:15:42.046Z" },
        { report_picture: null, clinic_name: "cat killer", clinic_phone_no: "87654321", vet_name: 'chopper', cat_id: catIds[1], report_date: "2021-01-04T03:15:42.046Z" },
        { report_picture: null, clinic_name: "cat killer", clinic_phone_no: "87654321", vet_name: 'chopper', cat_id: catIds[0], report_date: "2021-01-04T03:15:42.046Z" },
    ]).returning('id')

    let supplementIds = await knex("supplement").insert([
        { supplement_list: 'Aluminium Hydrogel', supplement_link: 'https://www.amazon.com/s?k=aluminum+hydrogel' },
        { supplement_list: 'Amino Plus', supplement_link:"https://www.amazon.com/s?k=Amino+Plus&i=pets&ref=nb_sb_noss"},
        { supplement_list: 'Azodyl', supplement_link:"https://www.amazon.com/s?k=azodyl&i=pets&ref=nb_sb_noss_2"},
        { supplement_list: 'Calcium Carbonate',supplement_link:"https://www.amazon.com/s?k=Calcium+Carbonate" },
        { supplement_list: 'Digestive Enzymes', supplement_link:"https://www.amazon.com/s?k=Digestive+Enzymes" },
        { supplement_list: 'Fucoidan', supplement_link:"https://www.amazon.com/s?k=Fucoidan" },
        { supplement_list: 'Ipakitine', supplement_link:"https://www.amazon.com/s?k=Ipakitine&ref=nb_sb_noss_2" },
        { supplement_list: 'Liposomal Vitamin C', supplement_link:"https://www.amazon.com/s?k=Liposomal+Vitamin+C&ref=nb_sb_noss_2" },
        { supplement_list: 'Milk Thistle', supplement_link:"https://www.amazon.com/s?k=Milk+Thistle&ref=nb_sb_noss_2" },
        { supplement_list: 'OMEGA-3', supplement_link:"https://www.amazon.com/s?k=OMEGA-3&ref=nb_sb_noss_2" },
        { supplement_list: 'SAM-e', supplement_link:"https://www.amazon.com/s?k=SAM-e&ref=nb_sb_noss_2" },
        { supplement_list: 'Taurine', supplement_link:"https://www.amazon.com/s?k=Taurine&ref=nb_sb_noss_2" },
        { supplement_list: 'Vitamin B', supplement_link:"https://www.amazon.com/s?k=Vitamin+B&ref=nb_sb_noss_2"},
        { supplement_list: 'Vitamin E', supplement_link:"https://www.amazon.com/s?k=Vitamin+e&ref=nb_sb_noss_2"},
    ]).returning('id');

    const bloodTestParametersIds = await knex('blood_test_parameters').insert([
        { parameter: "ALB", largerThan: "4", smallerThan: "2.2", supplement_id: supplementIds[4] },
        { parameter: "ALKP", largerThan: "111", smallerThan: "14", supplement_id: supplementIds[8] },
        { parameter: "ALT", largerThan: "130", smallerThan: "12", supplement_id: supplementIds[10] },
        { parameter: "AMYL", largerThan: "1500", smallerThan: "500", supplement_id: supplementIds[9] },
        { parameter: "BUN", largerThan: "36", smallerThan: "16", supplement_id: supplementIds[2] },
        { parameter: "CA", largerThan: "2.83", smallerThan: "1.95", supplement_id: supplementIds[3] },
        { parameter: "CHOL", largerThan: "5.81", smallerThan: "1.68", supplement_id: supplementIds[4] },
        { parameter: "CL", largerThan: "129", smallerThan: "112", supplement_id: supplementIds[12] },
        { parameter: "CREA", largerThan: "2.4", smallerThan: "0.8", supplement_id: supplementIds[6] },
        { parameter: "GLOB", largerThan: "51", smallerThan: "28", supplement_id: supplementIds[9] },
        { parameter: "GLU", largerThan: "8.84", smallerThan: "4.11", supplement_id: supplementIds[1] },
        { parameter: "K", largerThan: "5.8", smallerThan: "3.5", supplement_id: supplementIds[6] },
        { parameter: "NA", largerThan: "165", smallerThan: "150", supplement_id: supplementIds[7] },
        { parameter: "PHOS", largerThan: "2.42", smallerThan: "1", supplement_id: supplementIds[0] },
        { parameter: "TBIL", largerThan: "15", smallerThan: "0", supplement_id: supplementIds[11] },
        { parameter: "TP", largerThan: "89", smallerThan: "57", supplement_id: supplementIds[4] },
        { parameter: "UREA", largerThan: "12.9", smallerThan: "5.7", supplement_id: supplementIds[5] },
        { parameter: "HCL", largerThan: "48", smallerThan: "31", supplement_id: supplementIds[13] },
    ]).returning('id')

    await knex('report_data').insert([
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[0],
            value: 2.2
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[1],
            value: 14
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[2],
            value: 12
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[3],
            value: 500
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[4],
            value: 16
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[5],
            value: 1.95
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[6],
            value: 1.68
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[7],
            value: 112
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[8],
            value: 0.8
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[9],
            value: 28
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[10],
            value: 4.11
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[11],
            value: 3.5
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[12],
            value: 150
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[13],
            value: 2.42
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[14],
            value: 0
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[15],
            value: 57
        },
        {
            blood_test_report_id: bloodTestReportIds[0], blood_test_parameters_id: bloodTestParametersIds[16],
            value: 5.7
        },

        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[0],
            value: 2.2
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[1],
            value: 14
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[2],
            value: 12
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[3],
            value: 500
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[4],
            value: 16
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[5],
            value: 1.95
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[6],
            value: 1.68
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[7],
            value: 112
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[8],
            value: 0.8
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[9],
            value: 28
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[10],
            value: 4.11
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[11],
            value: 3.5
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[12],
            value: 150
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[13],
            value: 2.42
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[14],
            value: 0
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[15],
            value: 57
        },
        {
            blood_test_report_id: bloodTestReportIds[1], blood_test_parameters_id: bloodTestParametersIds[16],
            value: 5.7
        },

        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[0], value: 2.2 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[1], value: 14 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[2], value: 12 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[3], value: 500 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[4], value: 16 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[5], value: 1.95 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[6], value: 1.68 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[7], value: 112 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[8], value: 0.8 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[9], value: 28 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[10], value: 4.11 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[11], value: 3.5 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[12], value: 150 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[13], value: 2.42 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[14], value: 0 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[15], value: 57 },
        { blood_test_report_id: bloodTestReportIds[3], blood_test_parameters_id: bloodTestParametersIds[16], value: 5.7 },
        
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[0], value: 2.2 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[1], value: 14 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[2], value: 12 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[3], value: 500 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[4], value: 16 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[5], value: 1.95 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[6], value: 1.68 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[7], value: 112 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[8], value: 0.8 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[9], value: 28 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[10], value: 4.11 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[11], value: 3.5 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[12], value: 150 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[13], value: 2.42 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[14], value: 0 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[15], value: 57 },
        { blood_test_report_id: bloodTestReportIds[2], blood_test_parameters_id: bloodTestParametersIds[16], value: 5.7 },
    ])


};


