# CatSaver
A cat blood test report analyzing platform.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
<br><br>
<b>Prerequisites</b>
<br>you need to install npm or yarn, and PostgreSQL in your local machine to run this project. 
<br><br>
<b>Installing</b>
<br><br>

You may run below command to install all packages :
<br>

``` bash
yarn install
```
or
``` bash
npm install
```
<br>

<b>Setting Environment</b>
<br><br>
You need to create a .env file . You may follow .env.sample in the folders to create your .env file.

<br><br>You need to create a database in your PostgreSQL, and fill in the following in your .env file:<br><br>
example:

``` 
DB_NAME=your_database_name
DB_USERNAME=your_username
DB_PASSWORD=your_password
```

Then, you need to run below commands to create tables and insert records in your database.
```
yarn knex migrate:latest
yarn knex seed: run
```
<br>

<br>
<b>Starting the Program</b>
<br><br>
<b>PostgreSQL</b><br><br>
you may run below command to start PostgreSQL:

``` bash
sudo service postgresql start
```
<br>
<br>
you may run below command to start the API server:

```
yarn start
```

<br>

## Running the Tests

you may run the automated tests by below command:

```
yarn jest
```

## Authors

This project is done by below students from Tecky Academy:<br><br>
<ul>
<li>Charlie Tan
<li>Christina Wan
<li>Harriet Cheng
</ul>

## License

This project is licensed under MIT License.
