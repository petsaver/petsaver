import express from 'express';
// import { isLoggedIn } from './guards';
import { catController } from './main';
import multer from 'multer'

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `${__dirname}/public/uploads`);
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

const upload = multer({ storage });

export const catRoutes = express.Router();

catRoutes.get('/catprofile', catController.loadCatProfile)
catRoutes.get('/catprofile/:id', catController.loadCatProfileById)
catRoutes.get('/catreport/', catController.loadCatReport)
catRoutes.get('/catrecord/', catController.loadCatRecord)
catRoutes.get('/catBlood_type',catController.getAllBlood_types)
catRoutes.post('/createcat', upload.single('cat_pic'), catController.createCatProfile)
catRoutes.put('/updatecat/:id', upload.single('cat_pic'), catController.updateCatProfile)
catRoutes.delete('/deletecat/:id', catController.deleteCatProfile)
