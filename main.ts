import express from 'express';
import bodyParser from 'body-parser';
import expressSession from 'express-session';
import { isLoggedIn } from './guards';
import { UserService } from './services/UserService';
import { UserController } from './controllers/UserController';
import { CatService } from './services/CatService';
import { CatController } from './controllers/CatController';
import grant from 'grant';
import { NewsFeedService } from './services/NewsFeedService'
import { NewsFeedController } from './controllers/NewsFeedController'
import moment from 'moment'
import Knex from 'knex';
// import fs from 'fs';
// import path from 'path';

import { ReportController } from './controllers/ReportController';
import { ReportService } from './services/ReportService';
import { ReportResultController } from './controllers/ReportResultController';
import { ReportResultService } from './services/ReportResultService'


const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"])


const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(expressSession({
    secret: 'petSaver secret🤫',
    resave: true,
    saveUninitialized: true,
    cookie: { secure: false }
}));

const grantExpress = grant.express({
    "defaults": {
        "origin": "https://catsaver.site",
        // "origin": "http://localhost:8090",
        "transport": "session",
        "state": true,
    },
    "google": {
        "key": process.env.GOOGLE_CLIENT_ID || "",
        "secret": process.env.GOOGLE_CLIENT_SECRET || "",
        "scope": ["email"],
        "callback": "/login/google"
    }
})

app.use(grantExpress as express.RequestHandler);

app.use((req, res, next) => {
    console.log('[' + moment().format('yyyy-MM-DD HH:mm:ss') + '] ' + req.method + ' ' + req.path);
    next();
})

const userService = new UserService(knex);
export const userController = new UserController(userService);
const catService = new CatService(knex);
export const catController = new CatController(catService);
const newsFeedService = new NewsFeedService(knex)
const newsFeedController = new NewsFeedController(newsFeedService)
const reportService = new ReportService(knex);
export const reportController = new ReportController(reportService); 
const reportResultService = new ReportResultService(knex)
export const reportResultController = new ReportResultController(reportResultService)

// Data of NewsFeed
app.get('/newsfeed', newsFeedController.loadNewsFeed)

import { userRoutes } from './user_routes'
import { catRoutes } from './cat_routes'
import { reportRoutes } from './report_routes'

app.use('/', userRoutes)
app.use('/', catRoutes)
app.use('/', reportRoutes)
app.use(express.static('public'))

// app.use((req, res, next) => {
//     console.log(req.url)
//     if (!fs.existsSync("loggedIn" + req.url)) {
//         res.status(404)
//         res.sendFile(path.resolve('./public/404.html'))
//         return;
//     }
//     next()
// })

app.use(isLoggedIn, express.static('loggedIn'));

const PORT = 8090;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
    // if (process.env.CI) {
    //     process.exit(0);
    // }
});

