
loadUserProfile()
loadCatName()

async function loadUserProfile() {

    console.log("hello")

    let resUserData = await fetch('/profile');
    let userData = await resUserData.json();


    document.querySelector('.info-box').innerHTML = ""

    let userId = userData.id
    let userName = userData.username
    let userEmail = userData.email
    let userPhone_no = userData.phone_no
    let userDistrict = userData.district

    document.querySelector('.info-box').innerHTML += `
          <div class="card card-container">
          <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
          <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
          <p id="profile-name" class="profile-name-card"></p>
          <!-- <div class='background'> -->
              <form action="/updateProfile" method="PUT" class="form-register">
                  <span id="reauth-email" class="reauth-email"></span>
                  <div class="inputbox">
                  <div class="inputbox-item">Name:
                  </div>
                  <input type="text" id="inputUserName" name="catName" value=${userName} class="form-control"
                    required >
                    </div>
                <div class="custom-email inputbox">
                <div class="inputbox-item">
                Email:
                </div>
                <input type="text" id="inputEmail" name="catEmail" value=${userEmail} class="form-control disabled="disabled">
                </div>
                <div class="inputbox">
                <div class="inputbox-item">
                Phone:
                </div>
                <input type="text" id="inputPhone_no" name="catYear" value=${userPhone_no} class="form-control"
                    required>
                </div>
                <div class="inputbox">
                <div class="inputbox-item">
                District:
                </div>  
                        <select id="districts" class="custom-select form-control"> 
                        </select>
                    </div>    
                  <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" type="submit">Update</button>
              </form><!-- /form -->
          <!-- </div> -->
      </div><!-- /card-container -->
    `

    let districtsData = await fetch('/district')
    let districts = await districtsData.json()

    let districtHTML = document.getElementById("districts")

    districtHTML.innerHTML = ""
    for (let j = 0; j < districts.length; j++) {
        if (userDistrict == districts[j].district) {
            districtHTML.innerHTML += `<option selected class="${districts[j].district}">${districts[j].district}</option>`
        } else {
            districtHTML.innerHTML += `<option class="${districts[j].district}">${districts[j].district}</option>`
        }
    }
}



async function loadCatName() {
    console.log('loading cat data')
    let resCatData = await fetch('/catprofile');
    let catData = await resCatData.json();
    document.querySelector('.cat-profile-button').innerHTML = ""

    for (let i = 0; i < catData.length; i++) {
        let catId = catData[i].id
        let catName = catData[i].name
        document.querySelector('.cat-profile-button').innerHTML += `
        <a class="profile-button" onclick="loadCatProfile(${catId})">${catName}'s Profile</a>
        `
    }

}
async function loadCatProfile(id) {
    let resCatData = await fetch(`/catprofile/${id}`);
    let catData = await resCatData.json();
    document.querySelector('.info-box').innerHTML = ""

    let catId = catData[0].id
    let catName = catData[0].name
    let catGender = catData[0].gender
    let catYear = catData[0].date_of_birth.substring(0, 10)
    let catBreed = catData[0].breed
    let catBloodType = catData[0].type
    let catPic = catData[0].pet_pic

    let checkedMale = ""
    let checkedFemale = ""
    if (catGender == 'Male') {
        checkedMale = "checked"
    } else {
        checkedFemale = "checked"
    }

    document.querySelector('.info-box').innerHTML = `
    

          <div class="card card-container cat${catId}">
          <div id="result"></div>
        
          <!-- <div class='background'> -->
              <form class="form-register" id="update-cat" onsubmit="updateCat(event,${catId})">
                  <input type="file" name="cat_pic">
                  <div class="inputbox">Name:
                  <input type="text" id="inputcatName" name="catName" value=${catName} class="form-control"
                      required >
                    </div>
                    <div class="inputbox">Birth:
                      <input type="date" id="inputcatYear" name="catYear" value=${catYear} class="form-control"
                      required>
                      </div>
                      <div class="inputbox">Gender:
                <div class="custom-gender">
                    <input type="radio" id="catGenderMale" name="gender" value="Male" ${checkedMale}>
                    <label for="contactChoice1">Male</label>
                    <input type="radio" id="catGenderFemale" name="gender" value="Female" ${checkedFemale}>
                    <label for="contactChoice2">Female</label>
                    </div>
                </div>
                  <input type="text" id="inputcatBreed" name="catBreed" value=${catBreed} class="form-control" 
                    required>
                        <select id="blood_type_${catId}" class="custom-select" name="catBlood_type">
                        </select>
                  <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" >Update</button>
                  </form><!-- /form -->
                  
                  <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" onclick="deleteCat(${catId})">Delete</button>

          <!-- </div> -->
      </div><!-- /card-container -->
    `

    let blood_typesData = await fetch('/catBlood_type');
    let blood_types = await blood_typesData.json();

    let blood_typeHTML = document.getElementById(`blood_type_${catId}`);

    blood_typeHTML.innerHTML = ""
    for (let m = 0; m < blood_types.length; m++) {
        if (catBloodType == blood_types[m].type) {
            blood_typeHTML.innerHTML += `<option selected class="${blood_types[m].type}" >${blood_types[m].type}</option>`
        } else {
            blood_typeHTML.innerHTML += `<option class="${blood_types[m].type}">${blood_types[m].type}</option>`
        }
    }

    async function updateCat (event,id) {
        console.log ("trigger update cat on submit function")
        event.preventDefault()
        await updateCatAction(id)
    }
    
    async function updateCatAction (id) {
        let updateForm = document.querySelector("#update-cat")
        let formData = new FormData ()
        formData.append('catName',updateForm.catName.value);
        formData.append('catYear',updateForm.catYear.value);
        formData.append('gender',updateForm.gender.value);
        formData.append('catBreed',updateForm.catBreed.value);
        formData.append('catBlood_type',updateForm.catBlood_type.value);
        formData.append('cat_pic',updateForm.cat_pic.files[0]);
        console.log ("formdata" + formData)
    
        let resCatData = await fetch(`/updatecat/${id}`, {
            method: "PUT",
            body: formData,
        });
        let catData = await resCatData.json();
        console.log ("catData" + catData) 
    }
}




async function addCatProfile () {
    document.querySelector('.info-box').innerHTML = `
    <div class="card card-container addcat">
        <form action="/createcat" method="POST" class="form-register" enctype="multipart/form-data" >
            <input type="file" name="cat_pic" required />
            <div class="inputbox">
                <h4>Name:</h4>
                <input type="text" id="inputcatName" name="name"  class="form-control"
                    required >
            </div>

            <div class="inputbox">
                <h4>Birth:</h4>
                <input type="date" id="inputcatYear" name="date_of_birth" class="form-control"
                    required>
            </div>

            <div class="inputbox">
                <h4>Gender:</h4>
                <div class="custom-gender">
                    <input type="radio" id="catGenderMale" name="gender" value="Male" >
                    <label for="contactChoice1">Male</label>
                    <input type="radio" id="catGenderFemale" name="gender" value="Female">
                    <label for="contactChoice2">Female</label>
                </div>
            </div>

            <div class="inputbox">
                <h4>Breed:</h4>
                <input type="text" id="inputcatBreed" name="breed"  class="form-control" 
                required>
            </div>
            <div class="inputbox">
                <h4>Blood Type:</h4>
                <select id="blood_type" class="custom-select" name="blood_type">
                    <option>A</option>
                    <option>B</option>
                    <option>AB</option>
                </select>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" id="custom-button" type="submit">Create</button>
        </form><!-- /form -->
    </div>
    `
}


async function deleteCat (id) {
    let resCatData = await fetch (`/deletecat/${id}`, {
        method: 'DELETE',
    });
    let CatData = await resCatData.json();

    console.log (`catData ${CatData}`)

    window.location.reload()
}



