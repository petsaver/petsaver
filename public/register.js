
let form = document.querySelector("#register-form")
form.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formObject = {};
    formObject['username'] = form.username.value;
    formObject['password'] = form.password.value;
    formObject['confirmPassword'] = form.confirmpassword.value;
    formObject['email'] = form.email.value;
    formObject['district'] = form.district.value;
    formObject['phone_no'] = form.phone_no.value;

    if (form.password.value !== form.confirmpassword.value) {
        document.querySelector(".message").innerHTML = "Password is not match."
    } else if (form.district.value == "not_selected") {
        document.querySelector(".message").innerHTML = "Please choose the district"
     } else {
        let resUserData = await fetch('/signup', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(formObject)
        });
        let userData = await resUserData.json();
        console.log(userData)
        if (userData == "Wrong Email") {
            document.querySelector(".message").innerHTML = "Email is already exist."
        } else {
            window.location.href = "/home.html"
        }
    }
})
