//Remember Me Function Start
$(function() {
 
    if (localStorage.chkbx && localStorage.chkbx != '') {
        $('#remember_me').attr('checked', 'checked');
        $('#xip_Name').val(localStorage.usrname);
        $('#xip_Password').val(localStorage.pass);
    } else {
        $('#remember_me').removeAttr('checked');
        $('#xip_Name').val('');
        $('#xip_Password').val('');
    }

    $('#remember_me').click(function() {

        if ($('#remember_me').is(':checked')) {
            // save username and password
            localStorage.usrname = $('#xip_Name').val();
            localStorage.pass = $('#xip_Password').val();
            localStorage.chkbx = $('#remember_me').val();
        } else {
            localStorage.usrname = '';
            localStorage.pass = '';
            localStorage.chkbx = '';
        }
    });
});
//Remember Me Function End
