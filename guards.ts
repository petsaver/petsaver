import {Request,Response,NextFunction} from 'express';



export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        next();
    }else{
        //for development
        // console.log(req.session.id);
        
        // res.redirect('/');
        res.redirect('/login.html');
        // next();
    }
}

