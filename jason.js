const data = [
    {
        value: '14.00',
        blood_test_report_id: 25,
        blood_test_parameters_id: 56,
        parameter: 'ALKP',
        cat_id: 18
    },
    {
        value: '14.00',
        blood_test_report_id: 26,
        blood_test_parameters_id: 56,
        parameter: 'ALKP',
        cat_id: 18
    },
    {
        value: '14.00',
        blood_test_report_id: 28,
        blood_test_parameters_id: 56,
        parameter: 'ALKP',
        cat_id: 18
    },
    {
        value: '70.00',
        blood_test_report_id: 29,
        blood_test_parameters_id: 56,
        parameter: 'ALKP',
        cat_id: 18
    },
    {
        value: '12.00',
        blood_test_report_id: 25,
        blood_test_parameters_id: 57,
        parameter: 'ALT',
        cat_id: 18
    },
    {
        value: '12.00',
        blood_test_report_id: 26,
        blood_test_parameters_id: 57,
        parameter: 'ALT',
        cat_id: 18
    },
    {
        value: '12.00',
        blood_test_report_id: 28,
        blood_test_parameters_id: 57,
        parameter: 'ALT',
        cat_id: 18
    },
    {
        value: '35.00',
        blood_test_report_id: 29,
        blood_test_parameters_id: 57,
        parameter: 'ALT',
        cat_id: 18
    },
    {
        value: '500.00',
        blood_test_report_id: 25,
        blood_test_parameters_id: 58,
        parameter: 'AMYL',
        cat_id: 18
    },
    {
        value: '500.00',
        blood_test_report_id: 26,
        blood_test_parameters_id: 58,
        parameter: 'AMYL',
        cat_id: 18
    },
    {
        value: '500.00',
        blood_test_report_id: 28,
        blood_test_parameters_id: 58,
        parameter: 'AMYL',
        cat_id: 18
    },
    {
        value: '649.00',
        blood_test_report_id: 29,
        blood_test_parameters_id: 58,
        parameter: 'AMYL',
        cat_id: 18
    },
    {
        value: '16.00',
        blood_test_report_id: 25,
        blood_test_parameters_id: 59,
        parameter: 'BUN',
        cat_id: 18
    },
    {
        value: '16.00',
        blood_test_report_id: 26,
        blood_test_parameters_id: 59,
        parameter: 'BUN',
        cat_id: 18
    },
    {
        value: '16.00',
        blood_test_report_id: 28,
        blood_test_parameters_id: 59,
        parameter: 'BUN',
        cat_id: 18
    },
    {
        value: '22.00',
        blood_test_report_id: 29,
        blood_test_parameters_id: 59,
        parameter: 'BUN',
        cat_id: 18
    },
    {
        value: '0.80',
        blood_test_report_id: 25,
        blood_test_parameters_id: 63,
        parameter: 'CREA',
        cat_id: 18
    },
    {
        value: '0.80',
        blood_test_report_id: 26,
        blood_test_parameters_id: 63,
        parameter: 'CREA',
        cat_id: 18
    },
    {
        value: '0.80',
        blood_test_report_id: 28,
        blood_test_parameters_id: 63,
        parameter: 'CREA',
        cat_id: 18
    },
    {
        value: '0.00',
        blood_test_report_id: 25,
        blood_test_parameters_id: 69,
        parameter: 'TBIL',
        cat_id: 18
    },
    {
        value: '0.00',
        blood_test_report_id: 26,
        blood_test_parameters_id: 69,
        parameter: 'TBIL',
        cat_id: 18
    },
    {
        value: '0.00',
        blood_test_report_id: 28,
        blood_test_parameters_id: 69,
        parameter: 'TBIL',
        cat_id: 18
    },
    {
        value: '2.00',
        blood_test_report_id: 29,
        blood_test_parameters_id: 69,
        parameter: 'TBIL',
        cat_id: 18
    }
]

console.log(data)

const mapping = {};
const prefixStr = "Report ";
for (const row of data) {
    const param = row["parameter"];
    if (param in mapping) {
        // yau
        mapping[param]["data"].push(Number(row.value))
        const prefixStr = "Report ";
        mapping[param]["labels"].push(prefixStr + row.blood_test_report_id)
    } else {
        // mo
        mapping[param] = {
            data: [Number(row.value)],
            labels: [prefixStr + row.blood_test_report_id]
        }
    }
}
console.log(mapping)

const result = [];
for (let key in mapping) {
    result.push({
        name: key,
        data: mapping[key].data,
        labels: mapping[key].labels
    })
}
console.log(result);



//   [
//       {
//           name
//           data
//           labels
//       }
//   ]