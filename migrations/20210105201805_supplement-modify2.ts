import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('supplement',(table)=> {
        table.string('supplement_link');
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('supplement', table => {
        table.dropColumn('supplement_link')
    })
}

