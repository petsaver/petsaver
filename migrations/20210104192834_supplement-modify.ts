import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('blood_test_parameters', function(table) {
        table.decimal('largerThan').alter();
        table.decimal('smallerThan').alter();

    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('blood_test_parameters', function(table) {
        table.integer('largerThan').alter();
        table.integer('smallerThan').alter();
        
    })
}

