import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('report_data', table=>{
        table.dropForeign(['user_id'])
        table.dropColumn('user_id')
        table.integer('blood_test_parameters_id')
        table.foreign('blood_test_parameters_id').references('blood_test_parameters.id')
    })  
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('report_data', table=>{
        table.integer('user_id')
        table.foreign('user_id').references(`user.id`)
        table.dropForeign(['blood_test_parameters_id'])
        table.dropColumn('blood_test_parameters_id')
    })  
}

