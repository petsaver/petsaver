import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('news',(table)=>{
        table.text('source')
        table.text('author')
        table.text('url')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('news',(table)=>{
        table.dropColumn('source')
        table.dropColumn('author')
        table.dropColumn('url')
    })
}

