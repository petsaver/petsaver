import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.table('blood_test_parameters',(table)=> {
        table.decimal('largerThan');
        table.decimal('smallerThan');
        table.integer('supplement_id')
        table.foreign('supplement_id').references(`supplement.id`)
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('blood_test_parameters', table =>{
        table.dropForeign(['supplement_id'])
        table.dropColumn('supplement_id')
        table.dropColumn('smallerThan')
        table.dropColumn('largerThan')
    })  
}


