import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('news', table => {
        table.increments()
        table.text('title')
        table.text('description')
        table.text('urlToImage')
        table.timestamp('publishedAt')
        table.timestamps(false, true)
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('news')
}

