import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('blood_test_report', table=>{
        table.timestamp('report_date')
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('blood_test_report', table=>{
        table.dropColumn('report_date')
    })
}

