import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('district', (table) => {
        table.increments()
        table.string('district').notNullable()
        table.timestamps(false, true)
    })
    await knex.schema.alterTable('user', (table) => {
        table.unique(['email'])
        table.dropColumn('gender')
        table.dropColumn('date_of_birth')
        table.dropColumn('district')
        table.integer('district_id')
        table.foreign('district_id').references(`district.id`)
    })
    await knex.schema.createTable('blood_type', table => {
        table.increments()
        table.string('type')
        table.timestamps(false, true)
    })
    await knex.schema.createTable('cat', table => {
        table.increments()
        table.string('name')
        table.text('pet_pic')
        table.string('gender')
        table.date('date_of_birth')
        table.integer('blood_type_id')
        table.foreign('blood_type_id').references(`blood_type.id`)
        table.string('breed')
        table.integer('user_id')
        table.foreign('user_id').references(`user.id`)
        table.boolean('is_delete').defaultTo(false)
        table.timestamps(false, true)
    })
    await knex.schema.createTable('supplement', table => {
        table.increments()
        table.text('supplement_list')
        table.timestamps(false, true)
    })
    await knex.schema.createTable('blood_test_parameters', table =>{
        table.increments()
        table.string('parameter').notNullable()
        table.timestamps(false, true)
    })
    await knex.schema.createTable('report_data', table =>{
        table.increments()
        table.integer('user_id')
        table.foreign('user_id').references(`user.id`)
        table.decimal('value').unsigned()
        table.timestamps(false, true)
    })
    await knex.schema.createTable('blood_test_report', table =>{
        table.increments()
        table.text('report_picture')
        table.string('clinic_name')
        table.string('clinic_phone_no')
        table.string('vet_name')
        table.integer('report_data_id')
        table.foreign('report_data_id').references(`report_data.id`)
        table.integer('cat_id')
        table.foreign('cat_id').references(`cat.id`)
        table.timestamps(false, true)
    })
    await knex.schema.createTable('blood_test_report_supplement', table =>{
        table.increments()
        table.integer('supplement_id')
        table.foreign('supplement_id').references(`supplement.id`)
        table.integer('blood_test_report_id')
        table.foreign('blood_test_report_id').references(`blood_test_report.id`)
        table.timestamps(false, true)
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('user', (table) => {
        table.dropUnique(['email'])
        table.dropForeign(['district_id'])
        table.dropColumn('district_id')
        table.string('gender')
        table.date('date_of_birth')
        table.string('district')
    })
    await knex.schema.dropTable('report_data')
    await knex.schema.dropTable('blood_test_report')
    await knex.schema.dropTable('blood_test_report_supplement')
    await knex.schema.dropTable('cat')
    await knex.schema.dropTable('district')
    await knex.schema.dropTable('blood_type')
    await knex.schema.dropTable('supplement')
    await knex.schema.dropTable('blood_test_parameters')
}

