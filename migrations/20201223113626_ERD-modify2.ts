import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {  
    await knex.schema.alterTable('blood_test_report', table=>{
        table.dropForeign(['report_data_id'])
        table.dropColumn('report_data_id')
    })   
    await knex.schema.alterTable('report_data', table=>{
        table.integer('blood_test_report_id')
        table.foreign('blood_test_report_id').references('blood_test_report.id')
    })   

}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('blood_test_report', table=>{
        table.integer('report_data_id')
        table.foreign('report_data_id').references(`report_data.id`)
    })   
    await knex.schema.alterTable('report_data', table=>{
        table.dropForeign(['blood_test_report_id'])
        table.dropColumn('blood_test_report_id')
    })
}

